#ifndef PCH_H
#define PCH_H

#define _USE_MATH_DEFINES

#define MAIN_PC 1

constexpr int DEFAULT_WINDOW_WIDTH = 1280;
constexpr int DEFAULT_WINDOW_HEIGHT = 720;

#if MAIN_PC
constexpr int TARGET_FRAME_RATE = 144;
#else
constexpr int TARGET_FRAME_RATE = 60;
#endif


#endif
