#ifndef HEX_TILE_H
#define HEX_TILE_H

#include "Point.h"

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/ConvexShape.hpp>


class HexTile : public sf::Drawable
{
public:
	HexTile(Point gridPosition, sf::Vector2f position, sf::Color color, float size);
	~HexTile() = default;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	const sf::Color GetColor() const;
	void SetColor(const sf::Color newColor);

	void Select();
	void Deselect();

	const Point GetGridPosition() const;
	const sf::Vector2f GetPosition() const;

	static bool Compare (HexTile* lhs, HexTile* rhs) { return *lhs > *rhs; }

	friend bool operator< (const HexTile& lhs, const HexTile& rhs) { return lhs.m_position.y < rhs.m_position.y; }
	friend bool operator> (const HexTile& lhs, const HexTile& rhs) { return rhs < lhs; }
	friend bool operator<= (const HexTile& lhs, const HexTile& rhs) { return !(lhs > rhs); }
	friend bool operator>= (const HexTile& lhs, const HexTile& rhs) { return !(lhs < rhs); }
	friend bool operator== (const HexTile& lhs, const HexTile& rhs) { return lhs.m_gridPosition == rhs.m_gridPosition; }

private:

	const sf::Vector2f GetCornerPos(float i);

	sf::ConvexShape m_shape;
	sf::Vector2f m_position;
	Point m_gridPosition;
	float m_size;
	float m_radius;
};

#endif
