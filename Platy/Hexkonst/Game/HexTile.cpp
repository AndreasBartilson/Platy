#include "pch.h"
#include "HexTile.h"

#include "Graphics/Colors.h"

#include <math.h>
#include <SFML/Graphics/RenderTarget.hpp>

HexTile::HexTile(const Point gridPosition, const sf::Vector2f position, sf::Color color, const float size) :
	m_position(position),
	m_size(size),
	m_radius(size / 2.f),
	m_shape(sf::ConvexShape(6)),
	m_gridPosition(gridPosition)
{
	for (size_t i = 0; i < 6; i++)
	{
		m_shape.setPoint(i, GetCornerPos((float)i));
	}
	m_shape.setFillColor(color);
	m_shape.setOutlineThickness(m_size * .05f);
	m_shape.setOutlineColor(C_GREY);
}

void HexTile::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.texture = nullptr;
	target.draw(m_shape, states);
}

const sf::Color HexTile::GetColor() const
{
	return m_shape.getFillColor();
}

void HexTile::SetColor(const sf::Color newColor)
{
	m_shape.setFillColor(newColor);
}

void HexTile::Select()
{
	m_shape.setOutlineColor(C_WHITE);
}

void HexTile::Deselect()
{
	m_shape.setOutlineColor(C_GREY);
}

const Point HexTile::GetGridPosition() const
{
	return m_gridPosition;
}

const sf::Vector2f HexTile::GetPosition() const
{
	return m_position;
}

const sf::Vector2f HexTile::GetCornerPos(float i)
{
	float deg = 60 * i;
	float rad = (float)M_PI / 180 * deg;
	return sf::Vector2f(m_position.x + m_radius * (float)cos(rad), m_position.y + m_radius * (float)sin(rad));
}
