#ifndef HEX_GRID_H
#define HEX_GRID_H

constexpr auto GRID_SIZE = 10;
constexpr auto GRID_OFFSET_X = 200;
constexpr auto GRID_OFFSET_Y = 70;

#include "Point.h"
#include "Postmaster/Subscriber.h"

#include <vector>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Drawable.hpp>

class HexTile;

// Hexagonal "odd-q" grid
class HexGrid : public sf::Drawable, public Platy::Game::Subscriber
{
public:
	HexGrid(float tileSize, sf::Vector2u screenDimensions);
	~HexGrid() override;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	void ReceiveMessage(const Platy::Game::EMessageType& aMessageType) override;
	void ReceiveMessage(const Platy::Game::Message& aMessage, const Platy::Game::EMessageType& aMessageType) override;

private:

	void RotateTrio(std::vector<HexTile*> tiles, const bool rotateRight);
	void GenerateGrid();

	const sf::Color RandomColor() const;

	HexTile* PixelToHex(const sf::Vector2i position);
	std::vector<HexTile*> GetNeighbors(HexTile* origin);
	std::vector<HexTile*> GetClosestDuo(HexTile* origin, sf::Vector2i mousePosition);

	std::vector<std::vector<Point>> m_oddQDirections;
	std::vector<std::vector<HexTile*>> m_grid;
	std::vector<HexTile*> m_selectedTiles;
	float m_tileSize;
	float m_xOffset;
	float m_yOffset;

};

#endif
