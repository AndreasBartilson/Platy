#include "pch.h"
#include "HexGrid.h"

#include "HexTile.h"
#include "Util/Util.h"
#include "Graphics/Colors.h"
#include "Postmaster/Message.h"
#include "Postmaster/MessageType.h"

#include <map>
#include <iostream>
#include <Util/Util.h>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Graphics/RenderTarget.hpp>


using namespace Platy;

HexGrid::HexGrid(float tileSize, sf::Vector2u screenDimensions) : m_tileSize(tileSize), m_grid(), m_selectedTiles(), m_oddQDirections()
{
	m_xOffset = .5f * (screenDimensions.x - m_tileSize * (GRID_SIZE - 1));
	m_yOffset = .5f * (screenDimensions.y - m_tileSize * (GRID_SIZE - 1));

	Subscriber::Subscribe(Game::EMessageType::MOUSE_LEFT_RELEASED);
	Subscriber::Subscribe(Game::EMessageType::KEY_RELEASED);
	
	m_oddQDirections.push_back(std::vector<Point>());
	m_oddQDirections.push_back(std::vector<Point>());

	// Even cols
	m_oddQDirections[0].push_back(Point(1, 0));
	m_oddQDirections[0].push_back(Point(1, -1));
	m_oddQDirections[0].push_back(Point(0, -1));
	m_oddQDirections[0].push_back(Point(-1, -1));
	m_oddQDirections[0].push_back(Point(-1, 0));
	m_oddQDirections[0].push_back(Point(0, 1));

	// Odd cols
	m_oddQDirections[1].push_back(Point(1, 1));
	m_oddQDirections[1].push_back(Point(1, 0));
	m_oddQDirections[1].push_back(Point(0, -1));
	m_oddQDirections[1].push_back(Point(-1, 0));
	m_oddQDirections[1].push_back(Point(-1, 1));
	m_oddQDirections[1].push_back(Point(0, 1));

	GenerateGrid();
}

HexGrid::~HexGrid()
{
	for (auto& it : m_grid)
	{
		for (auto* tile : it)
		{
			Core::Util::SafeDelete(tile);
		}
	}

	Subscriber::RemoveAllSubscriptions();
}

void HexGrid::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.texture = nullptr;
	for (size_t i = 0; i < m_grid.size(); i++)
	{
		for (size_t j = 0; j < m_grid[i].size(); j++)
		{
			target.draw(*m_grid[i][j], states);
		}
	}
}

void HexGrid::ReceiveMessage(const Game::EMessageType& aMessageType)
{
}

void HexGrid::ReceiveMessage(const Game::Message& aMessage, const Game::EMessageType& aMessageType)
{
	HexTile* clickedTile;
	std::vector<HexTile*> closest2;

	sf::Vector2i mousePos;

	switch (aMessageType)
	{
	case Game::EMessageType::MOUSE_LEFT_RELEASED:
		
		// Deselect and clear previously selected tiles
		for (auto* it : m_selectedTiles)
		{
			it->Deselect();
		}
		m_selectedTiles.clear();

		mousePos = aMessage.GetPosition();

		// Acquire clicked tile
		clickedTile = PixelToHex(mousePos);
		if (clickedTile == nullptr)
		{
			break;
		}

		// Acquire clicked tile's closest 2 neighbors
		closest2 = GetClosestDuo(clickedTile, mousePos);
		if (closest2.size() == 0)
		{
			break;
		}

		clickedTile->Select();
		m_selectedTiles.push_back(clickedTile);
		
		for (auto* it : closest2)
		{
			it->Select();
			m_selectedTiles.push_back(it);
		}

		break;

	case Game::EMessageType::KEY_RELEASED:

		switch (aMessage.GetKey())
		{
		case sf::Keyboard::A:
			if (m_selectedTiles.size() != 0)
			{
				RotateTrio(m_selectedTiles, false);
			}
			break;
		case sf::Keyboard::D:
			if (m_selectedTiles.size() != 0)
			{
				RotateTrio(m_selectedTiles, true);
			}
			break;
		default:
			break;
		}

		break;
	default:
		break;
	}
}

void HexGrid::RotateTrio(std::vector<HexTile*> tiles, const bool rotateRight)
{
	std::sort(tiles.begin(), tiles.end(), HexTile::Compare);

	// TODO: Simplify?
	if (rotateRight)												// Hex-tri orientation
	{
		if (tiles[0]->GetPosition().x > tiles[1]->GetPosition().x)
		{  
			auto c0 = tiles[0]->GetColor();							//		  __
			tiles[0]->SetColor(tiles[2]->GetColor());				//     __/0 \ 
			auto c1 = tiles[1]->GetColor();							//    /1 \__/
			tiles[1]->SetColor(c0);									//    \__/2 \ 
			tiles[2]->SetColor(c1);									//       \__/
		}
		else
		{
			auto c0 = tiles[0]->GetColor();							//		__
			tiles[0]->SetColor(tiles[1]->GetColor());				//	   /0 \__
			auto c1 = tiles[2]->GetColor();							//     \__/1 \ 
			tiles[2]->SetColor(c0);									//     /2 \__/
			tiles[1]->SetColor(c1);									//     \__/
		}
	}
	else
	{
		if (tiles[0]->GetPosition().x > tiles[1]->GetPosition().x)
		{
			auto c0 = tiles[0]->GetColor();							//		  __
			tiles[0]->SetColor(tiles[1]->GetColor());				//     __/0 \ 
			auto c1 = tiles[2]->GetColor();							//    /1 \__/
			tiles[2]->SetColor(c0);									//    \__/2 \ 
			tiles[1]->SetColor(c1);									//       \__/
		}
		else															
		{
			auto c0 = tiles[0]->GetColor();							//		__
			tiles[0]->SetColor(tiles[2]->GetColor());				//	   /0 \__
			auto c1 = tiles[1]->GetColor();							//     \__/1 \ 
			tiles[1]->SetColor(c0);									//     /2 \__/
			tiles[2]->SetColor(c1);									//     \__/
		}
	}
}

void HexGrid::GenerateGrid()
{
	for (size_t i = 0; i < GRID_SIZE; i++)
	{
		m_grid.push_back(std::vector<HexTile*>());
		for (size_t j = 0; j < GRID_SIZE; j++)
		{
			float x = i * m_tileSize * .9f + m_xOffset;
			float y = i % 2 == 0 ? j * m_tileSize : j * m_tileSize + m_tileSize / 2;
			y += m_yOffset;

			HexTile* tile = new HexTile(Point((float)i, (float)j), sf::Vector2f(x, y), RandomColor(), m_tileSize);

			m_grid[i].push_back(tile);
		}
	}
}

HexTile* HexGrid::PixelToHex(const sf::Vector2i position)
{
	double xSpacing = (double)m_tileSize * .9;
	double ySpacing = m_tileSize;

	// Calculate using cube coordinates
	double x = ((double)position.x - m_xOffset) / xSpacing;
	double y = ((double)position.y - m_yOffset) / ySpacing;
	double z = -.5f * x - y;
	y = -.5f * x + y;

	int ix = (int)floor(x + .5);
	int iy = (int)floor(y + .5);
	int iz = (int)floor(z + .5);
	int s = ix + iy + iz;
	
	if (s)
	{
		double abs_dx = fabs(ix - x);
		double abs_dy = fabs(iy - y);
		double abs_dz = fabs(iz - z);
		if (abs_dx >= abs_dy && abs_dx >= abs_dz)
		{
			ix -= s;
		}
		else if (abs_dy >= abs_dx && abs_dy >= abs_dz)
		{
			iy -= s;
		}
		else
		{
			iz -= s;
		}
	}

	int yc = (iy - iz + (1 - ix % 2)) / 2;

	//std::cout << "X: " << ix << "  Y: " << yc << std::endl;
	
	if (ix >= 0 && ix < GRID_SIZE && yc >= 0 && yc < GRID_SIZE)
	{
		return m_grid[ix][yc];
	}

	return nullptr;
}

std::vector<HexTile*> HexGrid::GetNeighbors(HexTile* tile)
{
	std::vector<HexTile*> neighbors = std::vector<HexTile*>();

	Point origin = tile->GetGridPosition();
	int parity = (int)origin.GetX() % 2; // Whether we're in an odd or even column

	for (size_t i = 0; i < 6; i++)
	{
		Point p = m_oddQDirections[parity][i] + origin;
		if (p.GetX() >= 0 && p.GetX() < GRID_SIZE && p.GetY() >= 0 && p.GetY() < GRID_SIZE)
		{
			neighbors.push_back(m_grid[(size_t)p.GetX()][(size_t)p.GetY()]);
		}
	}

	return neighbors;
}

std::vector<HexTile*> HexGrid::GetClosestDuo(HexTile* origin, sf::Vector2i mousePosition)
{
	std::vector<HexTile*> closest = std::vector<HexTile*>();

	std::vector<float> distances;
	std::map<float, HexTile*> tileAndDistance;

	sf::Vector2f a = sf::Vector2f(mousePosition);
	for (auto* it : GetNeighbors(origin))
	{
		sf::Vector2f b = it->GetPosition();
		float x = b.x - a.x;
		float y = b.y - a.y;
		float d = sqrtf(x*x + y*y);
		tileAndDistance.emplace(d, it);
		distances.push_back(d);
	}

	std::sort(distances.begin(), distances.end());

	// The way the grid is ordered, a given tile always has two neighbors
	HexTile* first = tileAndDistance[distances[0]];
	HexTile* second = tileAndDistance[distances[1]];

	// Make sure two closest neighbors are each other's neighbor
	auto firstNeighbours = GetNeighbors(first);
	if (std::find(firstNeighbours.begin(), firstNeighbours.end(), second) != firstNeighbours.end())
	{
		closest.push_back(first);
		closest.push_back(second);
	}

	return closest;
}

const sf::Color HexGrid::RandomColor() const
{
	switch (Platy::Core::Util::RandInt(0, 5))
	{
	case 0:
		return C_BLUE;
	case 1:
		return C_RED;
	case 2:
		return C_GREEN_DARK;
	case 3:
		return C_ORANGE;
	case 4:
		return C_PURPLE;
	default:
		return C_CYAN; // Should never occur
	}
}
