#ifndef POINT_H
#define POINT_H

class Point
{
public:

	Point(float x, float y);
	~Point() = default;

	const float abs() const;

	Point operator+ (const Point& rhs) { return Point(x + rhs.x, y + rhs.y); }

	friend bool operator== (const Point& lhs, const Point& rhs) { return lhs.x == rhs.x && lhs.y == rhs.y; }
	friend bool operator<  (const Point& lhs, const Point& rhs) { return lhs.abs() < rhs.abs();}
	friend bool operator>  (const Point& lhs, const Point& rhs) { return rhs < lhs;}
	friend bool operator<= (const Point& lhs, const Point& rhs) { return !(lhs > rhs); }
	friend bool operator>= (const Point& lhs, const Point& rhs) { return !(lhs < rhs); }

	float GetX();
	float GetY();

private:
	float x, y;
};

#endif
