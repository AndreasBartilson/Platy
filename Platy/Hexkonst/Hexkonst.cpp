#include "pch.h"

#include "Game/HexGrid.h"
#include "Game/HexTile.h"
#include "Events/EventHandler.h"
#include "Postmaster/PostMaster.h"

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Vertex.hpp>
#include <SFML/Graphics/VertexArray.hpp>


constexpr const char* GAME_NAME = "Hexkonst";

int main()
{
	srand((unsigned)time(nullptr));

	sf::RenderWindow window;
	window.create(sf::VideoMode(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT), GAME_NAME);
	window.setVerticalSyncEnabled(false);
	window.setFramerateLimit(TARGET_FRAME_RATE);

	//sf::Image icon;
	//icon.loadFromFile("Assets/icon.png");
	//window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());

	/// Init clock for delta time
	sf::Clock tempClock;
	float deltaTime{};

	// Init singletons
	Platy::Game::PostMaster::Init();
	/*
	Platy::Log::Init();


	AssetContainer::Init();

	// Init game
	Game tempGame;
	*/

	HexGrid grid = HexGrid(60, sf::Vector2u(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT));


	while (window.isOpen())
	{
		Platy::Game::EventHandler::HandleEvent(window);

		deltaTime = tempClock.restart().asSeconds();

		//////////////////////////////////////////////////////////////////
		// Update logic
		//////////////////////////////////////////////////////////////////

		//ParticleManager::Update(deltaTime);

		//tempGame.Update(deltaTime);

		//////////////////////////////////////////////////////////////////
		// Draw logic
		//////////////////////////////////////////////////////////////////
		window.clear();

		//ParticleManager::EarlyDraw(window);

		window.draw(grid);

		//ParticleManager::Draw(window);

		window.display();
		//////////////////////////////////////////////////////////////////
	}
}