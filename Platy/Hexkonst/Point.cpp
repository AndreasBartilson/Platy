#include "Point.h"

#include <math.h>

Point::Point(float x, float y) : x(x), y(y)
{
}

const float Point::abs() const
{
    return sqrtf(x*x + y*y);
}

float Point::GetX()
{
    return x;
}

float Point::GetY()
{
    return y;
}
