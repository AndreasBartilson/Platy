#include "Matrix.h"

namespace Platy
{
	namespace Core
	{
		template <typename T>
		Matrix<T>::Matrix(const unsigned long long rows, const unsigned long long cols) : myCols(cols)
		{
			myArr = std::vector<T>(rows * cols);
		}

		template <typename T>
		Matrix<T>::Matrix(const Matrix& other) noexcept : myArr(other.myArr), myCols(other.myCols)
		{
		}

		template <typename T>
		Matrix<T>::Matrix(Matrix&& other) noexcept
		{
			myCols = std::move(other.myCols);
			myArr = std::move(other.myArr);
		}

		template <typename T>
		Matrix<T>& Matrix<T>::operator=(const Matrix& other) noexcept
		{
			myCols = other.myCols;
			myArr = other.myArr;
			return *this;
		}

		template <typename T>
		Matrix<T>& Matrix<T>::operator=(Matrix&& other) noexcept
		{
			if (this != other)
			{
				myCols = std::move(other.myCols);
				myArr = std::move(other.myArr);
			}

			return *this;
		}

		template <typename T>
		T* Matrix<T>::operator[](const int row)
		{
			return &myArr[row * myCols];
		}
	}
}
