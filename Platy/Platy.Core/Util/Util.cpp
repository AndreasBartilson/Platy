#include "Util.h"

#include <algorithm>
#include <ctime>

namespace Platy
{
	namespace Core
	{
		std::string Util::GetTime()
		{
			time_t tempRawTime;
			struct tm tempTimeInfo{};
			char buffer[44];

			time(&tempRawTime);
			localtime_s(&tempTimeInfo, &tempRawTime);

			strftime(buffer, sizeof(buffer), "%d-%m-%Y %H-%M-%S", &tempTimeInfo);
			std::string str(buffer);
			return str;
		}

		void Util::ToLowerCase(std::string& aString)
		{
			std::transform(aString.begin(), aString.end(), aString.begin(),
			               [](const unsigned char c) { return std::tolower(c); });
		}

		int Util::RandInt(const int min, const int max)
		{
			return std::rand() % max + min;
		}

		bool Util::RandBool()
		{
			return rand() % 2 == 0;
		}

		float Util::RandFloat(const float min, const float max)
		{
			return min + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (max - min)));
		}

		void Util::ClampDeg(float& anAngle)
		{
			if (anAngle > 360)
			{
				anAngle -= 360;
			}
			else if (anAngle < 0)
			{
				anAngle += 360;
			}
		}
	}
}
