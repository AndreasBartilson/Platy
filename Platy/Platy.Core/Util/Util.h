#ifndef UTIL_H
#define UTIL_H

#include <string>

namespace Platy
{
	namespace Core
	{
		class Util
		{
		public:
			Util() = delete;
			~Util() = default;

			/// <summary>
			/// Gets current time in a d-m-y h-m-s format
			/// </summary>
			static std::string GetTime();

			/// <summary>
			/// Safely deletes a pointer
			/// </summary>
			template <typename C>
			static void SafeDelete(C*& aPtrToDelete)
			{
				delete aPtrToDelete;
				aPtrToDelete = nullptr;
			}

			static void ToLowerCase(std::string& aString);

			static int RandInt(int min, int max);

			static bool RandBool();
			
			static float RandFloat(float min, float max);

			static void ClampDeg(float& anAngle);

			template <typename T>
			static void Clamp(T min, T max, T& value)
			{
				if (value < min)
				{
					value = min;
				}
				else if (value > max)
				{
					value = max;
				}
			}
		};
	}
}
#endif
