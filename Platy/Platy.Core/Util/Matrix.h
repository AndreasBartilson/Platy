#ifndef MATRIX_H
#define MATRIX_H

#include <vector>

namespace Platy
{
	namespace Core
	{
		template <typename T>
		class Matrix
		{
		public:
			// Default
			Matrix(unsigned long long rows, unsigned long long cols);
			// Copy
			Matrix(const Matrix& other) noexcept;
			// Move
			Matrix(Matrix&& other) noexcept;
			~Matrix() = default;

			// Copy
			Matrix& operator=(const Matrix& other) noexcept;
			// Move
			Matrix& operator=(Matrix&& other) noexcept;
			
			T* operator[](int row);

		private:
			std::vector<T> myArr;
			int myCols;
		};
	}
}
#endif
