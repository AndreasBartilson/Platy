#include "AssetContainer.h"

#include <fstream>
#include <Platy.Core/Util/Util.h>
#include <Platy.Log/Log.h>
#include <SFML/Graphics/Font.hpp>

using namespace Platy::Core;

namespace Platy
{
	namespace Game
	{
		constexpr const char* FILE_PATHS_FILE_FONTS = "Assets/File_paths_Fonts.txt";
		constexpr const char* FILEPATH_FONTS = "Assets/Fonts/";
		constexpr const char* FILE_PATHS_FILE_TEXTURES = "Assets/File_paths_Textures.txt";
		constexpr const char* FILEPATH_TEXTURES = "Assets/Textures/Sprites/";
		constexpr const char* FILE_PATHS_FILE_SPRITE_SHEETS = "Assets/File_paths_SpriteSheets.txt";
		constexpr const char* FILEPATH_SPRITE_SHEETS = "Assets/Textures/SpriteSheets/";

		std::map<std::string, sf::Texture*> AssetContainer::myTextures;
		std::map<std::string, Graphics::SpriteSheet*> AssetContainer::mySpriteSheets;
		std::map<std::string, sf::Font*> AssetContainer::myFonts;

		AssetContainer::~AssetContainer()
		{
			for (auto& it : myTextures)
			{
				Util::SafeDelete(it.second);
			}
			for (auto& it : mySpriteSheets)
			{
				Util::SafeDelete(it.second);
			}
			for (auto& it : myFonts)
			{
				Util::SafeDelete(it.second);
			}
		}

		void AssetContainer::Init()
		{
			// Load textures
			Load(FILE_PATHS_FILE_TEXTURES, myTextures,
			     [](std::string& line, std::map<std::string, sf::Texture*>& collection,
			        std::ifstream&)
			     {
				     auto* tempTexture = new sf::Texture;
				     if (!tempTexture->loadFromFile(FILEPATH_TEXTURES + line))
				     {
					     Log::Critical("AssetContainer<sf::Texture*, LoadFunction>(): Unable to load \"" + line + "\"");
					     Util::SafeDelete(tempTexture);
					     return;
				     }
				     collection.emplace(std::pair<std::string, sf::Texture*>(TrimFileName(line), tempTexture));
			     });

			// Load fonts
			Load(FILE_PATHS_FILE_FONTS, myFonts, [](std::string& line,
			                                        std::map<std::string, sf::Font*>& collection,
			                                        std::ifstream&)
			{
				auto* tempFont = new sf::Font;
				if (!tempFont->loadFromFile(FILEPATH_FONTS + line))
				{
					Log::Critical("AssetContainer<sf::Font*, LoadFunction>(): Unable to load \"" + line + "\"");
					Util::SafeDelete(tempFont);
					return;
				}
				collection.emplace(std::pair<std::string, sf::Font*>(TrimFileName(line), tempFont));
			});

			// Load sprite sheets
			Load(FILE_PATHS_FILE_SPRITE_SHEETS, mySpriteSheets,
			     [](std::string& line,
			        std::map<std::string, Graphics::SpriteSheet*>& collection,
			        std::ifstream& file)
			     {
				     auto* tempTexture = new sf::Texture;
				     if (!tempTexture->loadFromFile(FILEPATH_SPRITE_SHEETS + line))
				     {
					     Log::Critical(
						     "AssetContainer<Graphics::SpriteSheet*, LoadFunction>(): Unable to load \"" + line + "\"");
					     Util::SafeDelete(tempTexture);
					     for (size_t i = 0; i < 4; i++)
					     {
						     std::getline(file, line);
					     }
					     return;
				     }

				     const auto& name = TrimFileName(line);

				     // Get frame count
				     std::getline(file, line);
				     const auto tempFrameCount = std::stoi(line);

				     // Get frame rate
				     std::getline(file, line);
				     const auto tempFrameRate = std::stoi(line);

				     // Get rows
				     std::getline(file, line);
				     const auto tempRows = std::stoi(line);

				     // Get columns
				     std::getline(file, line);
				     const auto tempCols = std::stoi(line);

				     // Create sprite sheet
				     auto* tempSheet = new Graphics::SpriteSheet(tempTexture, tempFrameCount, tempFrameRate,
				                                                 tempRows, tempCols);

				     collection.emplace(std::pair<std::string, Graphics::SpriteSheet*>(name, tempSheet));
			     });
		}

		sf::Font* AssetContainer::GetFont(const std::string& name)
		{
			return GetByName<sf::Font>(myFonts, name);
		}

		sf::Texture* AssetContainer::GetTexture(const std::string& name)
		{
			return GetByName<sf::Texture>(myTextures, name);
		}

		Graphics::SpriteSheet* AssetContainer::GetSpriteSheet(const std::string& name)
		{
			return GetByName<Graphics::SpriteSheet>(mySpriteSheets, name);
		}

		template <typename T, typename LoadFunction>
		void AssetContainer::Load(const std::string& aFilePath, std::map<std::string, T*>& aCollection,
		                          LoadFunction aLoadFunc)
		{
			std::ifstream tempFile(aFilePath);
			if (!tempFile.is_open())
			{
				Log::Warning(
					"AssetContainer::Load<" + std::string(typeid(T).name()) +
					", LoadFunction>(): Unable to open file \"" + aFilePath + "\"");
				return;
			}

			std::string tempLine;
			unsigned tempTotalAssets = 0;
			while (std::getline(tempFile, tempLine))
			{
				tempTotalAssets++;
				aLoadFunc(tempLine, aCollection, tempFile);
			}
			tempFile.close();

			if (tempTotalAssets != 0)
			{
				Log::Information(
					"AssetContainer::Load<" + std::string(typeid(T).name()) + ", LoadFunction>(): " + std::to_string(
						aCollection.size())
					+ "/" + std::to_string(tempTotalAssets) + " assets loaded");
			}
		}

		template <typename T>
		T* AssetContainer::GetByName(std::map<std::string, T*>& aMap, std::string aName)
		{
			Util::ToLowerCase(aName);
			try
			{
				return aMap[aName];
			}
			catch (const std::exception& e)
			{
				Log::Warning(e, std::string(typeid(T).name()) + "* with aName: \"" + aName + "\" not found");
				return nullptr;
			}
		}

		std::string AssetContainer::TrimFileName(std::string& path)
		{
			path.erase(0, path.find_last_of('/') + 1);
			path.erase(path.find_last_of('.'));
			Util::ToLowerCase(path);
			return path;
		}
	}
}
