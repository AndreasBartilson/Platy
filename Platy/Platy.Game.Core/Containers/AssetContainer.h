#ifndef ASSET_CONTAINER_H
#define ASSET_CONTAINER_H

#include <map>
#include <string>

#include "Graphics/Spritesheet.h"

namespace sf
{
	class Texture;
	class Font;
}

namespace Platy
{
	namespace Game
	{
		class AssetContainer
		{
		public:
			AssetContainer() = delete;
			~AssetContainer();

			static void Init();

			static sf::Texture* GetTexture(const std::string& name);
			static Graphics::SpriteSheet* GetSpriteSheet(const std::string& name);
			static sf::Font* GetFont(const std::string& name);

		private:

			static std::map<std::string, sf::Texture*> myTextures;
			static std::map<std::string, Graphics::SpriteSheet*> mySpriteSheets;
			static std::map<std::string, sf::Font*> myFonts;

			template <typename T, typename LoadFunction>
			static void Load(const std::string& aFilePath, std::map<std::string, T*>& aCollection,
			                 LoadFunction aLoadFunc);

			template <typename T>
			static T* GetByName(std::map<std::string, T*>& aMap, std::string aName);

			static std::string TrimFileName(std::string& path);
		};
	}
}
#endif
