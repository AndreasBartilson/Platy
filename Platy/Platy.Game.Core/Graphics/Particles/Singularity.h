#ifndef SINGULARITY_H
#define SINGULARITY_H

#include "ParticleEmitter.h"

namespace Platy
{
	namespace Game
	{
		class Singularity : public ParticleEmitter
		{
		public:
			Singularity(
				const sf::Vector2f& aPosition,
				const sf::Color& aColor,
				const size_t& aParticleCount,
				const float& aParticleMaxSize,
				const float& aRadius);

			~Singularity() override = default;

			void Update(const float& someDeltaTime) override;

		protected:
			void ResetParticle(const size_t& anIndex) override;

		private:

			sf::Vector2f GetSingularityPosition() const;

			float myRadius;
		};
	}
}
#endif
