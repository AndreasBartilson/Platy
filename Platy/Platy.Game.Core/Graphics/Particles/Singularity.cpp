#include "Singularity.h"

#include "Platy.Game.Core/Util/Util.h"
#include "Util/Util.h"


namespace Platy
{
	namespace Game
	{
		Singularity::Singularity(const sf::Vector2f& aPosition,
		                         const sf::Color& aColor,
		                         const size_t& aParticleCount,
		                         const float& aParticleMaxSize,
		                         const float& aRadius)
			:
			ParticleEmitter(
				aParticleMaxSize, EOrientation(), aPosition, aColor, aParticleCount, 0, 2, 0, 0, 0, 1, true,
				false),
			myRadius(aRadius)
		{
		}

		void Singularity::Update(const float& someDeltaTime)
		{
			for (size_t i = 0; i < myParticles.size(); i++)
			{
				auto& p = myParticles.at(i);
				p.lifeTime -= someDeltaTime;
				if (p.lifeTime <= 0)
				{
					ResetParticle(i);
					continue;
				}

				for (size_t j = 0; j < 4; j++)
				{
					myParticleVertices[i * 4 + j].position = Util::Lerp(myParticleVertices[i * 4 + j].position,
					                                                    myPosition,
					                                                    myInitLifeTime * someDeltaTime);

					myParticleVertices[i * 4 + j].color.a = static_cast<sf::Uint8>(Util::Lerp(
						myParticleVertices[i * 4 + j].color.a, 255, myInitLifeTime * someDeltaTime));
				}
			}
		}

		void Singularity::ResetParticle(const size_t& anIndex)
		{
			myParticleVertices[anIndex * 4].position = GetSingularityPosition();
			ParticleEmitter::ResetParticle(anIndex);
			for (size_t i = 0; i < 4; i++)
			{
				myParticleVertices[anIndex * 4 + i].color.a = 0;
			}
		}

		sf::Vector2f Singularity::GetSingularityPosition() const
		{
			return myPosition + Util::DegToVec2(Core::Util::RandFloat(0, 360)) * myRadius;
		}
	}
}
