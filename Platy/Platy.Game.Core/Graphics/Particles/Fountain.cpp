#include "Fountain.h"

#include "Platy.Game.Core/Util/Util.h"

#include <Util/Util.h>

namespace Platy
{
	namespace Game
	{
		Fountain::Fountain(
			const float& aParticleMaxSize,
			const sf::Vector2f& aPosition,
			const sf::Color& aColor,
			const size_t& aParticleCount,
			const float& anIntensity,
			const float& aLifeTime,
			const float& anEmissionAngle,
			const float& aSpreadAngle,
			const float& someGravity,
			const bool& isShortLived,
			const bool& shouldFade)
			: ParticleEmitter(
				  aParticleMaxSize,
				  EOrientation(),
				  aPosition,
				  aColor,
				  aParticleCount,
				  anIntensity,
				  aLifeTime,
				  0,
				  anEmissionAngle,
				  someGravity,
				  INTENSITY_MODULATION_DIVIDER_FOUNTAIN,
				  shouldFade,
				  isShortLived),
			  mySpreadAngle(aSpreadAngle)
		{
			if (isShortLived)
			{
				for (size_t i = 0; i < myParticles.size(); i++)
				{
					ParticleEmitter::ResetParticle(i);
				}
			}
		}

		void Fountain::ResetParticle(const size_t& anIndex)
		{
			myParticles[anIndex].velocity = Util::DegToVec2(
				Core::Util::RandFloat(myEmissionAngle - mySpreadAngle, myEmissionAngle + mySpreadAngle));
			myParticleVertices[anIndex * 4].position = myPosition;
			ParticleEmitter::ResetParticle(anIndex);
		}
	}
}
