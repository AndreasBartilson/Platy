#ifndef PARTICLE_MANAGER_H
#define PARTICLE_MANAGER_H

#define ASYNC 0

#if ASYNC
#include <future>
#endif

#include <vector>

namespace sf
{
	class RenderWindow;
}

namespace Platy
{
	namespace Game
	{
		class ParticleEmitter;

		constexpr unsigned PARTICLE_COUNT_MAX = 10000;

		class ParticleManager final
		{
		public:
			ParticleManager() = delete;
			~ParticleManager();

			static void Update(const float& someDeltaTime);
			static void EarlyDraw(sf::RenderWindow& aWindow);
			static void Draw(sf::RenderWindow& aWindow);
			static void AddEmitter(ParticleEmitter* anEmitter, bool isEarly = false);
			static void RemoveAllEffects();

			static size_t GetParticleCount();

		private:

			static std::vector<ParticleEmitter*> myEarlyParticleEmitters;
			static std::vector<ParticleEmitter*> myParticleEmitters;
#if ASYNC
				static std::vector<std::future<void>> myFutures;
#endif
		};
	}
}
#endif
