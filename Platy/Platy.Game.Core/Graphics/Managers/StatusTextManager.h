#ifndef STATUS_TEXT_MANAGER_H
#define STATUS_TEXT_MANAGER_H

#include <vector>

namespace sf
{
	class RenderWindow;
}

namespace Platy
{
	namespace Game
	{
		class StatusText;

		class StatusTextManager
		{
		public:
			StatusTextManager() = delete;
			~StatusTextManager() = default;

			static void Update(float& someDeltaTime);
			static void Draw(sf::RenderWindow& aWindow);

			static void AddStatusText(const StatusText& aStatusText);
		private:

			static std::vector<StatusText> myStatusTexts;
		};
	}
}

#endif
