#include "StatusTextManager.h"

#include "StatusText.h"

#include <SFML/Graphics/RenderWindow.hpp>

namespace Platy
{
	namespace Game
	{
		std::vector<StatusText> StatusTextManager::myStatusTexts;

		void StatusTextManager::Update(float& someDeltaTime)
		{
			for (auto i = myStatusTexts.size(); i > 0; i--)
			{
				myStatusTexts.at(i - 1).Update(someDeltaTime);
				if (!myStatusTexts.at(i - 1).GetAlive())
				{
					myStatusTexts.erase(myStatusTexts.begin() + i - 1);
				}
			}
		}

		void StatusTextManager::Draw(sf::RenderWindow& aWindow)
		{
			for (const auto& it : myStatusTexts)
			{
				aWindow.draw(it);
			}
		}

		void StatusTextManager::AddStatusText(const StatusText& aStatusText)
		{
			myStatusTexts.push_back(aStatusText);
		}
	}
}
