#include "StatusTextFactory.h"

#include "Containers/AssetContainer.h"
#include "Graphics/Managers/StatusTextManager.h"
#include "StatusText.h"

#include <SFML/Graphics/Font.hpp>

namespace Platy
{
	namespace Game
	{
		sf::Font* StatusTextFactory::myDefaultFont;
		unsigned StatusTextFactory::myDefaultFontSize;

		void StatusTextFactory::Init(const std::string& aFontName, const unsigned aFontSize)
		{
			myDefaultFont = AssetContainer::GetFont(aFontName);
			myDefaultFontSize = aFontSize;
		}

		void StatusTextFactory::Create(const sf::Vector2f aPosition, const std::string& aText)
		{
			StatusTextManager::AddStatusText(StatusText(aPosition, myDefaultFont, myDefaultFontSize, aText));
		}
	}
}
