#ifndef STATUS_TEXT_FACTORY
#define STATUS_TEXT_FACTORY

#include <SFML/System/Vector2.hpp>
#include <string>

namespace sf
{
	class Font;
}

namespace Platy
{
	namespace Game
	{
		class StatusTextFactory
		{
		public:
			StatusTextFactory() = delete;
			~StatusTextFactory() = default;

			static void Init(const std::string& aFontName, unsigned aFontSize = 20);

			static void Create(sf::Vector2f aPosition, const std::string& aText);

			// TODO: Add more suitable factory methods

		private:

			static sf::Font* myDefaultFont;
			static unsigned myDefaultFontSize;
		};
	}
}
#endif
