#include "KeyboardEventHandler.h"

#include "Postmaster/PostMaster.h"
#include "Postmaster/Message.h"
#include "Postmaster/MessageType.h"

#include <SFML/Window/Event.hpp>


namespace Platy
{
	namespace Game
	{
		void KeyboardEventHandler::HandleEvent(const sf::Event& anEvent)
		{
			switch (anEvent.type)
			{
			case sf::Event::KeyPressed:
				HandleKeyPressed(anEvent);
				break;
			case sf::Event::KeyReleased:
				HandleKeyReleased(anEvent);
				break;
			default:
				break;
			}
		}

		void KeyboardEventHandler::HandleKeyPressed(const sf::Event& anEvent)
		{
			Message msg;
			msg.SetKey(anEvent.key.code);
			msg.SetBool(anEvent.key.shift);
			PostMaster::SendMessage(msg, EMessageType::KEY_PRESSED);
		}

		void KeyboardEventHandler::HandleKeyReleased(const sf::Event& anEvent)
		{
			Message msg;
			msg.SetKey(anEvent.key.code);
			msg.SetBool(anEvent.key.shift);
			PostMaster::SendMessage(msg, EMessageType::KEY_RELEASED);
		}
	}
}
