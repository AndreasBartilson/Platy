#include "MouseEventHandler.h"

#include "Postmaster/PostMaster.h"
#include "Postmaster/Message.h"
#include "Postmaster/MessageType.h"

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

namespace Platy
{
	namespace Game
	{
		void MouseEventHandler::HandleEvent(const sf::Event& anEvent, const sf::RenderWindow& aWindow)
		{
			Message msg(sf::Mouse::getPosition(aWindow));
			
			// TODO: Add suitable events
			switch (anEvent.type)
			{
			case sf::Event::MouseButtonPressed:

				switch (anEvent.mouseButton.button)
				{
				case sf::Mouse::Left:
					PostMaster::SendMessage(msg, EMessageType::MOUSE_LEFT_PRESSED);
					break;
				case sf::Mouse::Right:
					PostMaster::SendMessage(msg, EMessageType::MOUSE_RIGHT_PRESSED);
					break;
				default:
					break;
				}
				break;

			case sf::Event::MouseButtonReleased:

				switch (anEvent.mouseButton.button)
				{
				case sf::Mouse::Left:
					msg.SetBool(anEvent.key.control);
					PostMaster::SendMessage(msg, EMessageType::MOUSE_LEFT_RELEASED);
					break;
				case sf::Mouse::Right:
					PostMaster::SendMessage(msg, EMessageType::MOUSE_RIGHT_RELEASED);
					break;
				default:
					break;
				}
				break;

			case sf::Event::MouseMoved:
				PostMaster::SendMessage(msg, EMessageType::MOUSE_MOVED);
				break;

			case sf::Event::MouseEntered:
			case sf::Event::MouseLeft:
			case sf::Event::MouseWheelMoved:
			case sf::Event::MouseWheelScrolled:
				break;
			default:
				break;
			}
		}
	}
}
