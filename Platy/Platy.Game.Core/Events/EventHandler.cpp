#include "EventHandler.h"

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "Log.h"
#include "MouseEventHandler.h"
#include "GamePadEventHandler.h"
#include "KeyboardEventHandler.h"

namespace Platy
{
	namespace Game
	{
		sf::Event EventHandler::myEvent;

		void EventHandler::HandleEvent(sf::RenderWindow& aWindow)
		{
			while (aWindow.pollEvent(myEvent))
			{
				switch (myEvent.type)
				{
				case sf::Event::Closed:
					Log::Dispose();
					aWindow.close();
					break;

				case sf::Event::GainedFocus:
					break;

				case sf::Event::LostFocus:
					break;

				case sf::Event::KeyPressed:
				case sf::Event::KeyReleased:
					KeyboardEventHandler::HandleEvent(myEvent);
					break;

				case sf::Event::MouseButtonPressed:
				case sf::Event::MouseButtonReleased:
				case sf::Event::MouseEntered:
				case sf::Event::MouseLeft:
				case sf::Event::MouseMoved:
				case sf::Event::MouseWheelMoved:
				case sf::Event::MouseWheelScrolled:
					MouseEventHandler::HandleEvent(myEvent, aWindow);
					break;
				case sf::Event::JoystickButtonPressed:
				case sf::Event::JoystickButtonReleased:
				case sf::Event::JoystickMoved:
				case sf::Event::JoystickConnected:
				case sf::Event::JoystickDisconnected:
					GamePadEventHandler::HandleEvent(myEvent);
					break;
				case sf::Event::TextEntered:
				case sf::Event::Resized:
				case sf::Event::TouchBegan:
				case sf::Event::TouchMoved:
				case sf::Event::TouchEnded:
				case sf::Event::SensorChanged:
				case sf::Event::Count:
					break;
				}
			}
		}
	}
}
