#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

namespace sf
{
	class RenderWindow;
	class Event;
}

namespace Platy
{
	namespace Game
	{
		class EventHandler
		{
		public:
			EventHandler() = delete;
			~EventHandler() = default;

			static void HandleEvent(sf::RenderWindow& aWindow);

		private:
			static sf::Event myEvent;
		};
	}
}

#endif
