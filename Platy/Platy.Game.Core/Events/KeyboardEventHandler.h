#ifndef KEYBOARD_EVENT_HANDLER_H
#define KEYBOARD_EVENT_HANDLER_H

namespace sf
{
	class Event;
}

namespace Platy
{
	namespace Game
	{
		class KeyboardEventHandler
		{
		public:
			KeyboardEventHandler() = delete;
			~KeyboardEventHandler() = default;

			static void HandleEvent(const sf::Event& anEvent);

		private:
			static void HandleKeyPressed(const sf::Event& anEvent);
			static void HandleKeyReleased(const sf::Event& anEvent);
		};
	}
}
#endif
