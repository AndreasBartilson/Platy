#ifndef STATUS_TEXT_H
#define STATUS_TEXT_H

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Drawable.hpp>

namespace sf
{
	class Font;
}

namespace Platy
{
	namespace Game
	{
		class StatusText final : public sf::Drawable
		{
		public:
			StatusText(sf::Vector2f aPosition, sf::Font* aFont, unsigned aFontSize,
			           const std::string& aText,
			           sf::Color aColour = sf::Color(255, 255, 255),
			           float aLifeTime = 1.5f);

			~StatusText() override = default;

			void Update(float& someDeltaTime);
			bool GetAlive() const;

		protected:
			void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

		private:
			float myLifeTime;
			float myInitLifeTime;

			sf::Text myText;
			sf::Color myColour;
		};
	}
}
#endif
