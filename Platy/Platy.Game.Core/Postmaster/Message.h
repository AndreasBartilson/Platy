#ifndef MESSAGE_H
#define MESSAGE_H

#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Event.hpp>

namespace Platy
{
	namespace Game
	{
		class Message
		{
		public:
			Message();
			explicit Message(const sf::Vector2i& aPosition);
			Message(const sf::Vector2i& aPosition, int aWidth, int aHeight);
			~Message() = default;

			void SetFloat(float aValue);
			void SetInt(int aValue);
			void SetBool(bool aValue);
			void SetKey(sf::Keyboard::Key key);

			const sf::Vector2i& GetPosition() const;
			sf::Vector2i GetSize() const;
			const int& GetInt() const;
			const float& GetFloat() const;
			const bool& GetBool() const;
			sf::Keyboard::Key GetKey() const;

		private:

			sf::Vector2i myPosition;
			float myFloat;
			int myInt,
			    myWidth,
			    myHeight;
			bool myBool;
			sf::Keyboard::Key myKey;
		};
	}
}
#endif
