#include "StatusText.h"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

namespace Platy
{
	namespace Game
	{
		StatusText::StatusText(const sf::Vector2f aPosition, sf::Font* aFont, unsigned aFontSize,
		                       const std::string& aText,
		                       const sf::Color aColour,
		                       const float aLifeTime)
			:
			myLifeTime(aLifeTime),
			myInitLifeTime(aLifeTime),
			myText(aText, *aFont, aFontSize),
			myColour(aColour)
		{
			myText.setPosition(aPosition);
			myText.setFillColor(aColour);
		}

		void StatusText::Update(float& someDeltaTime)
		{
			myLifeTime -= someDeltaTime;
			myColour.a = static_cast<sf::Uint8>(255.f * (myLifeTime / myInitLifeTime));
			myText.setFillColor(myColour);
		}

		bool StatusText::GetAlive() const
		{
			return myLifeTime > 0;
		}

		void StatusText::draw(sf::RenderTarget& target, sf::RenderStates states) const
		{
			target.draw(myText);
		}
	}
}
