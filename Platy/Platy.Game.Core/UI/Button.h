#ifndef BUTTON_H
#define BUTTON_H

#include "Postmaster/Subscriber.h"

#include <string>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>

// TODO: Make font size adjust to button size

namespace Platy
{
	namespace Game
	{
		enum class EMessageType;

		class Button final : public sf::Drawable, public Subscriber
		{
		public:
			Button(sf::Vector2f aPosition, sf::Vector2f aSize, const std::string& someText,
			       const std::string& aFontName);
			~Button() override;

			void SetMessage(EMessageType aMessageType); // Change to string or something, maybe?

			void ReceiveMessage(const EMessageType& aMessageType) override;
			void ReceiveMessage(const Message& aMessage, const EMessageType& aMessageType) override;

			void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

		private:

			void InitSubscriptions();

			EMessageType myMessageType;

			sf::RectangleShape myRect;
			sf::Text myText;
		};
	}
}
#endif
