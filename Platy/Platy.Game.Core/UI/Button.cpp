#include "Button.h"

#include "Containers/AssetContainer.h"
#include "Graphics/Colors.h"
#include "Postmaster/Message.h"
#include "Postmaster/MessageType.h"

#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

namespace Platy
{
	namespace Game
	{
		Button::Button(const sf::Vector2f aPosition, const sf::Vector2f aSize, const std::string& someText,
		               const std::string& aFontName)
			:
			myMessageType(),
			myRect(aSize),
			myText(someText, *AssetContainer::GetFont(aFontName))
		{
			InitSubscriptions();

			myRect.setPosition(aPosition);

			myRect.setFillColor(C_CYAN_DARK);
			myRect.setOutlineColor(C_CYAN);
			myText.setPosition(aPosition);
			myText.setCharacterSize(static_cast<int>(aSize.y) - 15);
		}

		Button::~Button()
		{
			RemoveAllSubscriptions();
		}

		void Button::SetMessage(const EMessageType aMessageType)
		{
			myMessageType = aMessageType;
		}

		void Button::ReceiveMessage(const EMessageType& aMessageType)
		{
		}

		void Button::ReceiveMessage(const Message& aMessage, const EMessageType& aMessageType)
		{
			const auto tempIsMouseHovering = myRect.getGlobalBounds().intersects(
				sf::Rect<float>(sf::Vector2f(aMessage.GetPosition()), sf::Vector2f(1, 1)));

			switch (aMessageType)
			{
			case EMessageType::MOUSE_LEFT_PRESSED:
				if (tempIsMouseHovering)
				{
					SendMessage(myMessageType);
				}
				break;

			case EMessageType::MOUSE_MOVED:

				if (tempIsMouseHovering)
				{
					myRect.setOutlineThickness(3);
				}
				else
				{
					myRect.setOutlineThickness(0);
				}
				break;

			default:
				break;
			}
		}

		void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
		{
			target.draw(myRect);
			target.draw(myText);
		}

		void Button::InitSubscriptions()
		{
			Subscribe(EMessageType::MOUSE_LEFT_PRESSED);
			Subscribe(EMessageType::MOUSE_MOVED);
		}
	}
}
