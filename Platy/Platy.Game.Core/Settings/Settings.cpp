#include "Settings.h"

namespace Platy
{
	namespace Game
	{
		float Settings::myGamePadAxisDeadZone;

		void Settings::Init()
		{
			myGamePadAxisDeadZone = 25;
			// TODO: Load settings from file (make a default setting file or something)
		}

		void Settings::SetGamePadDeadZone(const float& aValue)
		{
			myGamePadAxisDeadZone = aValue;
		}

		const float& Settings::GetGamePadDeadZone()
		{
			return myGamePadAxisDeadZone;
		}
	}
}
