#include "Util.h"

#include <cmath>
#include <Util/Util.h>

namespace Platy
{
	namespace Game
	{
		sf::Vector2f Util::DegToVec2(const float& anAngle)
		{
			return {std::cos(anAngle * DEG_TO_RAD), sin(anAngle * DEG_TO_RAD)};
		}

		sf::Vector2f Util::Lerp(const sf::Vector2f& a, const sf::Vector2f& b, const float amount)
		{
			return {Lerp(a.x, b.x, amount), Lerp(a.y, b.y, amount)};
		}

		float Util::Lerp(const float& a, const float& b, const float& amount)
		{
			return a * (1.f - amount) + b * amount;
		}

		sf::Vector2f Util::RandVec2(const float aMinX, const float aMaxX, const float aMinY, const float aMaxY)
		{
			return {Core::Util::RandFloat(aMinX, aMaxX), Core::Util::RandFloat(aMinY, aMaxY)};
		}
		float Util::Abs(sf::Vector2f vector)
		{
			return sqrtf(vector.x * vector.x + vector.y * vector.y);
		}
	}
}
