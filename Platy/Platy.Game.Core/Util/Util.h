#ifndef GAME_UTIL_H
#define GAME_UTIL_H

#include <SFML/System/Vector2.hpp>

constexpr float DEG_TO_RAD = 0.0174533f;
constexpr float G = 9.82f;

namespace Platy
{
	namespace Game
	{
		class Util
		{
		public:
			Util() = delete;
			~Util() = default;

			static sf::Vector2f DegToVec2(const float& anAngle);

			static sf::Vector2f Lerp(const sf::Vector2f& a, const sf::Vector2f& b, float amount);

			static float Lerp(const float& a, const float& b, const float& amount);

			static sf::Vector2f RandVec2(float aMinX, float aMaxX, float aMinY, float aMaxY);

			static float Abs(sf::Vector2f vector);
		};
	}
}

#endif
