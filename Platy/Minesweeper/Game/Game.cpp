#include "Game.h"
#include "pch.h"

#include <Containers/AssetContainer.h>
#include <Graphics/Colors.h>
#include <Graphics/Managers/ParticleManager.h>
#include <Postmaster/Message.h>
#include <Postmaster/MessageType.h>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <Util/Util.h>
#include <Platy.Log/Log.h>

constexpr auto TILE_SIZE = 32;

using namespace Platy::Game;
using namespace Graphics;

Game::Game() :
	myState(EState::PLAYING),
	myMineCounterText("", *AssetContainer::GetFont("digital_characters")),
	myTimeCounterText("", *AssetContainer::GetFont("digital_characters"))
{
	myMineCounterText.setFillColor(C_RED);
	myMineCounterText.setCharacterSize(50);
	myMineCounterText.setPosition(200, 0);

	myTimeCounterText.setFillColor(C_RED);
	myTimeCounterText.setCharacterSize(50);
	myTimeCounterText.setPosition(500, 0);

	ResetMineField();
	UpdateMineCounterText();
	UpdateTimeCounterText();

	Subscribe(EMessageType::GAME_OVER);
	Subscribe(EMessageType::VICTORY);
	Subscribe(EMessageType::KEY_PRESSED);
	Subscribe(EMessageType::TILE_FLAGGED);
	Subscribe(EMessageType::TILE_UN_FLAGGED);
	Subscribe(EMessageType::MOUSE_LEFT_PRESSED);
	Subscribe(EMessageType::MOUSE_RIGHT_PRESSED);
	Subscribe(EMessageType::KEY_PRESSED);
	Subscribe(EMessageType::KEY_RELEASED);
}

Game::~Game()
{
	Platy::Core::Util::SafeDelete(myActiveMineField);
}

void Game::Update(const float& someDeltaTime)
{
	myActiveMineField->Update(someDeltaTime);
	UpdateTimeCounterText();
}

void Game::ReceiveMessage(const EMessageType& aMessageType)
{
	switch (aMessageType)
	{
	case EMessageType::GAME_OVER:
		break;
	case EMessageType::TILE_FLAGGED:
	case EMessageType::TILE_UN_FLAGGED:
		UpdateMineCounterText();
		break;
	default:
		break;
	}
}

void Game::ReceiveMessage(const Message& aMessage, const EMessageType& aMessageType)
{
	switch (aMessageType)
	{
	case EMessageType::KEY_PRESSED:
		
		if (aMessage.GetKey() == sf::Keyboard::Key::Escape)
		{
			ResetMineField();
		}
		else if (aMessage.GetKey() == sf::Keyboard::Key::LShift)
		{
			myActiveMineField->SetShiftDown(true);
		}

		break;
	case EMessageType::KEY_RELEASED:

		if (aMessage.GetKey() == sf::Keyboard::Key::LShift)
		{
			myActiveMineField->SetShiftDown(false);
		}
		break;

	case EMessageType::MOUSE_LEFT_PRESSED:
	case EMessageType::MOUSE_RIGHT_PRESSED:
		Platy::Log::Debug("Mouse button: " + std::string(aMessageType == EMessageType::MOUSE_RIGHT_PRESSED ? "right" : "left"));
		myActiveMineField->HandleClick(aMessage, aMessageType);
		break;
	default:
		break;
	}
}

void Game::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.texture = nullptr;
	switch (myState)
	{
	case EState::GAME_OVER:
	case EState::PAUSED:
	case EState::MENU:
		break;
	case EState::PLAYING:
		target.draw(*myActiveMineField);
		target.draw(myMineCounterText);
		target.draw(myTimeCounterText);
		break;
	}
}

void Game::ResetMineField()
{
	Platy::Core::Util::SafeDelete(myActiveMineField);
	ParticleManager::RemoveAllEffects();

	sf::Vector2i offset;
	switch (myChosenDifficulty)
	{
	case MineField::ESize::EASY:
		offset = GetOffset(COLS_EASY, ROWS_EASY);
		break;
	case MineField::ESize::INTERMEDIATE:
		offset = GetOffset(COLS_INTERMEDIATE, ROWS_INTERMEDIATE);
		break;
	case MineField::ESize::HARD:
		offset = GetOffset(COLS_HARD, ROWS_HARD);
		break;
	}

	myActiveMineField = new MineField(myChosenDifficulty, offset);

	UpdateMineCounterText();
}

void Game::UpdateMineCounterText()
{
	const auto text = myActiveMineField->GetNbrMinesLeft() > 9
		                  ? "0" + std::to_string(myActiveMineField->GetNbrMinesLeft())
		                  : "00" + std::to_string(myActiveMineField->GetNbrMinesLeft());
	myMineCounterText.setString(text);
}

void Game::UpdateTimeCounterText()
{
	std::string text = "000";
	if (myActiveMineField->GetElapsedSeconds() < 10 && myActiveMineField->GetElapsedSeconds() > 0)
	{
		text = "00" + std::to_string(myActiveMineField->GetElapsedSeconds());
	}
	else if (myActiveMineField->GetElapsedSeconds() < 100 && myActiveMineField->GetElapsedSeconds() > 9)
	{
		text = "0" + std::to_string(myActiveMineField->GetElapsedSeconds());
	}
	else if (myActiveMineField->GetElapsedSeconds() > 99)
	{
		text = std::to_string(myActiveMineField->GetElapsedSeconds());
	}
	myTimeCounterText.setString(text);
}

sf::Vector2i Game::GetOffset(const uint8_t cols, const uint8_t rows)
{
	return {
		DEFAULT_WINDOW_WIDTH / 2 - static_cast<int>(static_cast<float>(cols) / 2) * TILE_SIZE,
		DEFAULT_WINDOW_HEIGHT / 2 - static_cast<int>(static_cast<float>(rows) / 2) * TILE_SIZE
	};
}
