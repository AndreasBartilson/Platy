#ifndef MINEFIELD_H
#define MINEFIELD_H

#include "Tiles/Tile.h"

#include <vector>
#include <Postmaster/Subscriber.h>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Sprite.hpp>

constexpr unsigned int DEFAULT_FRAME_THICKNESS = 16;

constexpr uint8_t COLS_EASY = 9;
constexpr uint8_t ROWS_EASY = 9;
constexpr uint8_t COLS_INTERMEDIATE = 16;
constexpr uint8_t ROWS_INTERMEDIATE = 16;
constexpr uint8_t COLS_HARD = 30;
constexpr uint8_t ROWS_HARD = 16;

constexpr uint8_t MINES_EASY = 10;
constexpr uint8_t MINES_INTERMEDIATE = 40;
constexpr uint8_t MINES_HARD = 99;

class MineField final : public sf::Drawable
{
public:
	enum class ESize
	{
		EASY,
		INTERMEDIATE,
		HARD
	};

	enum class EState
	{
		SWEEPING,
		GAME_OVER,
		VICTORY
	};

	MineField() = default;
	MineField(ESize aSize, sf::Vector2i offset = sf::Vector2i(16, 16));
	~MineField() override;

	///////////////////////////////
	// GET
	///////////////////////////////

	unsigned GetElapsedSeconds() const;

	const uint8_t& GetNbrMinesLeft() const;

	///////////////////////////////

	
	void HandleClick(const Platy::Game::Message& aMessage, const Platy::Game::EMessageType& aMessageType);

	void Update(const float& someDeltaTime);

	void SetShiftDown(const bool value);

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

private:

	sf::Vector2i myOffset;
	sf::Sprite myFrame;

	std::vector<std::vector<Tile>> myMineField;

	std::vector<Tile*> myMinePointers;

	EState myState = EState::SWEEPING;

	uint8_t myNbrCols;
	uint8_t myNbrRows;
	uint8_t myNbrMines;
	uint8_t myNbrMinesLeft;

	float myElapsedTime;
	float myMineExplosionDelay;
	float myExplosionTimer;

	bool myShiftDown;

	void GenerateField(uint8_t cols, uint8_t rows, uint8_t aNbrMines);

	void PlaceMines(uint8_t aNbrMines);

	void RevealTile(sf::Vector2i aPos, bool recursive = false);

	bool InRange(const int& col, const int& row) const;

	bool CheckVictory() const;

	sf::Vector2i ToMapPos(const sf::Vector2i& aWindowPos) const;
};

#endif
