#include "MineField.h"

#include <Containers/AssetContainer.h>
#include <Graphics/Colors.h>
#include <Graphics/Factories/ParticleEmitterFactory.h>
#include <Postmaster/Message.h>
#include <Postmaster/MessageType.h>
#include <Postmaster/PostMaster.h>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <Util/Util.h>


using namespace Platy::Game;
using namespace Graphics;

MineField::MineField(const ESize aSize, const sf::Vector2i offset) :
	myOffset(offset),
	myNbrCols(),
	myNbrRows(),
	myNbrMines(),
	myNbrMinesLeft(),
	myElapsedTime(),
	myMineExplosionDelay(.02f),
	myExplosionTimer(),
	myShiftDown(false)
{
	myFrame.setPosition(sf::Vector2f(offset - sf::Vector2i(DEFAULT_FRAME_THICKNESS, DEFAULT_FRAME_THICKNESS)));
	switch (aSize)
	{
	case ESize::EASY:
		myFrame.setTexture(*AssetContainer::GetTexture("frame_easy"));
		GenerateField(COLS_EASY, ROWS_EASY, MINES_EASY);
		myNbrCols = COLS_EASY;
		myNbrRows = ROWS_EASY;
		myNbrMines = MINES_EASY;
		break;
	case ESize::INTERMEDIATE:
		myFrame.setTexture(*AssetContainer::GetTexture("frame_intermediate"));
		GenerateField(COLS_INTERMEDIATE, ROWS_INTERMEDIATE, MINES_INTERMEDIATE);
		myNbrCols = COLS_INTERMEDIATE;
		myNbrRows = ROWS_INTERMEDIATE;
		myNbrMines = MINES_INTERMEDIATE;
		break;
	case ESize::HARD:
		myFrame.setTexture(*AssetContainer::GetTexture("frame_hard"));
		GenerateField(COLS_HARD, ROWS_HARD, MINES_HARD);
		myNbrCols = COLS_HARD;
		myNbrRows = ROWS_HARD;
		myNbrMines = MINES_HARD;
		break;
	}
}

MineField::~MineField()
{
	myMinePointers.clear();
}

unsigned MineField::GetElapsedSeconds() const
{
	return static_cast<unsigned>(myElapsedTime);
}

const uint8_t& MineField::GetNbrMinesLeft() const
{
	return myNbrMinesLeft;
}


void MineField::HandleClick(const Message& aMessage, const EMessageType& aMessageType)
{
	if (myState != EState::SWEEPING)
	{
		return;
	}

	const auto pos = ToMapPos(aMessage.GetPosition());
	auto& tempTile = myMineField.at(pos.x).at(pos.y);
	switch (aMessageType)
	{
	case EMessageType::MOUSE_LEFT_PRESSED:

		if (tempTile.GetState() == Tile::EState::Unchecked)
		{
			RevealTile(pos);
		}
		else if (myShiftDown && tempTile.GetState() == Tile::EState::Checked)
		{
			auto closeFlagCount = 0;

			for (auto c = -1; c <= 1; c++)
			{
				for (auto r = -1; r <= 1; r++)
				{
					const auto tempCol = pos.x + c;
					const auto tempRow = pos.y + r;

					if ((tempCol != pos.x || tempRow != pos.y) && InRange(tempCol, tempRow) &&
						myMineField.at(tempCol).at(tempRow).GetState() == Tile::EState::Flagged)
					{
						closeFlagCount++;
					}
				}
			}

			if (closeFlagCount != tempTile.GetCloseMineCount())
			{
				return;
			}
			for (auto c = -1; c <= 1; c++)
			{
				for (auto r = -1; r <= 1; r++)
				{
					const auto tempCol = pos.x + c;
					const auto tempRow = pos.y + r;

					if ((tempCol != pos.x || tempRow != pos.y) && InRange(tempCol, tempRow) && 
						myMineField.at(tempCol).at(tempRow).GetState() == Tile::EState::Unchecked)
					{
						RevealTile(sf::Vector2i(tempCol, tempRow));
					}
				}
			}
		}

		break;

	case EMessageType::MOUSE_RIGHT_PRESSED:

		tempTile.ToggleState();
		switch (tempTile.GetState())
		{
		case Tile::EState::Flagged:
			if (myNbrMinesLeft > 0)
			{
				myNbrMinesLeft--;
			}

			PostMaster::SendMessage(EMessageType::TILE_FLAGGED);

			if (!CheckVictory())
			{
				return;
			}
			for (auto& col : myMineField)
			{
				for (auto& row : col)
				{
					if (row.GetState() == Tile::EState::Unchecked || row.GetState() == Tile::EState::Flagged)
					{
						row.Reveal();
					}
				}
			}
			myState = EState::VICTORY;
			PostMaster::SendMessage(EMessageType::VICTORY);
			
			break;
		case Tile::EState::Questioned:
			if (myNbrMinesLeft < myNbrMines)
			{
				myNbrMinesLeft++;
			}
			PostMaster::SendMessage(EMessageType::TILE_UN_FLAGGED);
			break;
		case Tile::EState::Checked:
		case Tile::EState::Unchecked:
			break;
		}
		break;
	default:
		break;
	}
}

void MineField::Update(const float& someDeltaTime)
{
	switch (myState)
	{
	case EState::GAME_OVER:
		myExplosionTimer -= someDeltaTime;
		if (myExplosionTimer <= 0 && !myMinePointers.empty())
		{
			myExplosionTimer = myMineExplosionDelay;
			const auto pos = myMinePointers.at(myMinePointers.size() - 1)->GetPosition() + sf::Vector2i(
				DEFAULT_TILE_SIZE / 2, DEFAULT_TILE_SIZE / 2);

			myMinePointers.at(myMinePointers.size() - 1)->Explode();
			myMinePointers.pop_back();

			ParticleEmitterFactory::CreateExplosion(4, sf::Vector2f(pos), C_YELLOW, 50, 200, .5f, false, 9.82f * 2);
			ParticleEmitterFactory::CreateExplosion(4, sf::Vector2f(pos), C_ORANGE, 50, 200, .5f, false, 9.82f * 2);
			ParticleEmitterFactory::CreateExplosion(4, sf::Vector2f(pos), C_RED, 50, 200, .5f, false, 9.82f * 2);
			ParticleEmitterFactory::CreateExplosion(4, sf::Vector2f(pos), C_RED_DARK, 50, 200, .5f, false,
			                                        9.82f * 2);
		}
		break;
	case EState::SWEEPING:
		myElapsedTime += someDeltaTime;
		break;
	case EState::VICTORY:
		break;
	}
}

void MineField::SetShiftDown(const bool value)
{
	myShiftDown = value;
}

void MineField::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.texture = nullptr;
	for (const auto& col : myMineField)
	{
		for (const auto& tile : col)
		{
			target.draw(tile);
		}
	}
	target.draw(myFrame);
}

void MineField::GenerateField(const uint8_t cols, const uint8_t rows, const uint8_t aNbrMines)
{
	myNbrCols = cols;
	myNbrRows = rows;

	for (auto i = 0; i < myNbrCols; i++)
	{
		myMineField.emplace_back();
		for (auto j = 0; j < myNbrRows; j++)
		{
			myMineField.at(i).emplace_back(sf::Vector2i(i, j) * DEFAULT_TILE_SIZE + myOffset);
		}
	}

	PlaceMines(aNbrMines);
}

void MineField::PlaceMines(const uint8_t aNbrMines)
{
	myNbrMines = myNbrMinesLeft = aNbrMines;

	for (size_t i = 0; i < myNbrMines; i++)
	{
		auto row = Platy::Core::Util::RandInt(0, myNbrRows);
		auto col = Platy::Core::Util::RandInt(0, myNbrCols);

		while (myMineField.at(col).at(row).HasMine())
		{
			row = Platy::Core::Util::RandInt(0, myNbrRows);
			col = Platy::Core::Util::RandInt(0, myNbrCols);
		}

		myMineField.at(col).at(row).PlaceMine();
		myMinePointers.push_back(&myMineField.at(col).at(row));

		// Add 1 mine count to neighboring tiles
		for (auto c = -1; c <= 1; c++)
		{
			for (auto r = -1; r <= 1; r++)
			{
				const auto tempCol = col + c;
				const auto tempRow = row + r;

				if ((tempCol != col || tempRow != row) && InRange(tempCol, tempRow) &&
					!myMineField.at(tempCol).at(tempRow).HasMine())
				{
					myMineField.at(tempCol).at(tempRow).AddCloseMine();
				}
			}
		}
	}
}

void MineField::RevealTile(const sf::Vector2i aPos, const bool recursive)
{
	if (myMineField.at(aPos.x).at(aPos.y).GetState() != Tile::EState::Unchecked)
	{
		return;
	}
	const auto info = myMineField.at(aPos.x).at(aPos.y).Reveal();

	if (info.isMine && !recursive)
	{
		for (auto i = 0; i < myNbrCols; i++)
		{
			for (auto j = 0; j < myNbrRows; j++)
			{
				auto& tempTile = myMineField.at(i).at(j);
				if (tempTile.HasMine())
				{
					tempTile.Reveal();
				}
			}
		}

		// Make sure flagged mines don't explode
		for (auto i = myMinePointers.size(); i > 0; i--)
		{
			if (myMinePointers.at(i - 1)->GetState() == Tile::EState::Flagged)
			{
				myMinePointers.at(i - 1) = nullptr;
				myMinePointers.erase(myMinePointers.begin() + i - 1);
			}
		}
		myState = EState::GAME_OVER;
		PostMaster::SendMessage(EMessageType::GAME_OVER);
	}
	else if (info.closeMineCount == 0)
	{
		for (auto c = -1; c <= 1; c++)
		{
			for (auto r = -1; r <= 1; r++)
			{
				const auto tempCol = aPos.x + c;
				const auto tempRow = aPos.y + r;

				if ((tempCol != aPos.x || tempRow != aPos.y) && InRange(tempCol, tempRow) &&
					myMineField.at(tempCol).at(tempRow).GetState() == Tile::EState::Unchecked)
				{
					RevealTile(sf::Vector2i(tempCol, tempRow), true);
				}
			}
		}
	}
	
}

bool MineField::InRange(const int& col, const int& row) const
{
	return col >= 0 &&
		col < myNbrCols &&
		row >= 0 &&
		row < myNbrRows;
}

bool MineField::CheckVictory() const
{
	for (auto* it : myMinePointers)
	{
		if (it->GetState() != Tile::EState::Flagged)
		{
			return false;
		}
	}
	return true;
}

sf::Vector2i MineField::ToMapPos(const sf::Vector2i& aWindowPos) const
{
	return
	{
		(aWindowPos.x - myOffset.x) / DEFAULT_TILE_SIZE,
		(aWindowPos.y - myOffset.y) / DEFAULT_TILE_SIZE
	};
}
