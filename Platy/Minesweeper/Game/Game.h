#ifndef GAME_H
#define GAME_H

#include "MineField.h"

#include <Postmaster/Subscriber.h>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Text.hpp>

class Game : public sf::Drawable, public Platy::Game::Subscriber
{
public:
	Game();
	~Game() override;

	enum class EState
	{
		GAME_OVER,
		MENU,
		PLAYING,
		PAUSED
	};

	void Update(const float& someDeltaTime);

	void ReceiveMessage(const Platy::Game::EMessageType& aMessageType) override;
	void ReceiveMessage(const Platy::Game::Message& aMessage, const Platy::Game::EMessageType& aMessageType) override;

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

private:

	EState myState;

	sf::Text myMineCounterText;
	sf::Text myTimeCounterText;

	MineField::ESize myChosenDifficulty = MineField::ESize::HARD;

	MineField* myActiveMineField;

	void ResetMineField();
	void UpdateMineCounterText();
	void UpdateTimeCounterText();

	static sf::Vector2i GetOffset(uint8_t cols, uint8_t rows);
};

#endif
