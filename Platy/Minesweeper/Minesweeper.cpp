#include "pch.h"
#include "Game/Game.h"

#include <Containers/AssetContainer.h>
#include <Events/EventHandler.h>
#include <Graphics/Managers/ParticleManager.h>
#include <Platy.Log/Log.h>
#include <Postmaster/PostMaster.h>
#include <SFML/Graphics.hpp>

constexpr const char* GAME_NAME = "Minesweeper";

using namespace Platy::Game;
using namespace Graphics;

int main()
{
	srand(static_cast<unsigned>(time(nullptr)));

	sf::RenderWindow window;
	window.create(sf::VideoMode(DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT), GAME_NAME);
	window.setVerticalSyncEnabled(false);
	window.setFramerateLimit(TARGET_FRAME_RATE);

	sf::Image icon;
	icon.loadFromFile("Assets/icon.png");
	window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());

	/// Init clock for delta time
	sf::Clock tempClock;
	float deltaTime{};

	// Init singletons
	Platy::Log::Init();

	PostMaster::Init();

	AssetContainer::Init();

	// Init game
	Game tempGame;


	while (window.isOpen())
	{
		EventHandler::HandleEvent(window);

		deltaTime = tempClock.restart().asSeconds();

		//////////////////////////////////////////////////////////////////
		// Update logic
		//////////////////////////////////////////////////////////////////

		ParticleManager::Update(deltaTime);

		tempGame.Update(deltaTime);

		//////////////////////////////////////////////////////////////////
		// Draw logic
		//////////////////////////////////////////////////////////////////
		window.clear();

		ParticleManager::EarlyDraw(window);

		window.draw(tempGame);

		ParticleManager::Draw(window);

		window.display();
		//////////////////////////////////////////////////////////////////
	}
}
