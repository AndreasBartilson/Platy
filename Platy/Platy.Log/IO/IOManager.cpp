#include "IOManager.h"

#include <iostream>
#include <Windows.h>
#include <Util/Util.h>

namespace Platy
{
	namespace IO
	{
		std::string IOManager::myFileName;
		std::ofstream IOManager::myFileStream;

		void IOManager::Init()
		{
			if (CreateDirectoryA("Logs", nullptr) == ERROR_PATH_NOT_FOUND)
			{
				std::cout << "IOManager::Init(): Could not create directory: Invalid path" << std::endl;
				return;
			}
			myFileName = "Logs\\" + Core::Util::GetTime() + ".txt";
			myFileStream = std::ofstream(myFileName);
		}

		void IOManager::Dispose()
		{
			if (myFileStream && myFileStream.is_open())
			{
				myFileStream.close();
				std::cout << "\nLog saved to: " << myFileName << std::endl;
			}
		}

		void IOManager::WriteToFile(const std::string& line)
		{
			if (myFileStream.is_open())
			{
				myFileStream << line << std::endl;
			}
		}
	}
}
