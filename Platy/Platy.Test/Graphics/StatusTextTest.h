#ifndef STATUS_TEXT_TEST_H
#define STATUS_TEXT_TEST_H

class StatusTextTest
{
public:
	StatusTextTest() = default;
	~StatusTextTest() = default;

	void Update(const float& someDeltaTime);

private:
	float myTextTimer;
};

#endif
