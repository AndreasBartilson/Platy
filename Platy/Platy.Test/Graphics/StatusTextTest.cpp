#include "StatusTextTest.h"

#include <Graphics/Factories/StatusTextFactory.h>
#include <Util/Util.h>

using namespace Platy::Game;


void StatusTextTest::Update(const float& someDeltaTime)
{
	myTextTimer -= someDeltaTime;

	if (myTextTimer <= 0)
	{
		myTextTimer = 1;
		StatusTextFactory::Create(
			Util::RandVec2(200, 1000, 200, 700),
			"This is a test");
	}
}
