#include "ParticleTest.h"

#include <SFML/Graphics/Color.hpp>
#include <Graphics/Colors.h>
#include <Graphics/Factories/ParticleEmitterFactory.h>
#include <Util/Util.h>
#include <Graphics/Particles/Enum.h>

using namespace Platy::Game;

ParticleTest::ParticleTest()
	:
	myExplosionTimer(0),
	myExplosionInterval(1)
{
	ParticleEmitterFactory::CreateSingularity(sf::Vector2f(640, 360), C_WHITE, 500, 10, 50);
	//ParticleEmitterFactory::CreateSingularity(sf::Vector2f(640, 360), C_AMETIST, 2000, 20, 700);

	ParticleEmitterFactory::CreateCloud(30, sf::Vector2f(90, 50), C_GREY_LIGHT, 1500, 5, 2.5f,
	                                    720);
	ParticleEmitterFactory::CreateShower(4, EOrientation::DOWN, sf::Vector2f(100, 50), C_BLUE, 3000, 200, 2, 700,
	                                     true,
	                                     90, 9.82f * 2 * 2);
	ParticleEmitterFactory::CreateFountain(4, sf::Vector2f(600, 700), C_CYAN, 1500, 200, 1.f, 270, 35);
	ParticleEmitterFactory::CreateFountain(20, sf::Vector2f(800, 700), C_CYAN, 200, 350, 1.5f, 270, 35, false, false,
	                                       9.82f * 2);
}

void ParticleTest::Update(const float& someDeltaTime)
{
	myExplosionTimer -= someDeltaTime;
	if (myExplosionTimer <= 0)
	{
		ParticleEmitterFactory::CreateExplosion(
			4, Util::RandVec2(600, 1200, 100, 600),
			C_ORANGE, 1000, 300, 1, false, 9.82f * 2);
		myExplosionTimer = myExplosionInterval;
	}
}
