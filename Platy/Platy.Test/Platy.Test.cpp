#define TEST_CORE 0
#define TEST_GAME_CORE 1
#define TEST_LOG 0

#include "Graphics/StatusTextTest.h"
#include "Graphics/ParticleTest.h"

#include <SFML/Graphics.hpp>
#include <Postmaster/PostMaster.h>
#include <Log.h>
#include <Containers/AssetContainer.h>
#include <Events/EventHandler.h>
#include <Graphics/Animator.h>
#include <Graphics/Factories/StatusTextFactory.h>
#include <Graphics/Managers/ParticleManager.h>
#include <Graphics/Managers/StatusTextManager.h>
#if TEST_CORE
#include <Platy.Test/CoreTests/TestObservable.h>
#include <Platy.Test/CoreTests/TestObserver.h>
#endif
#include <Settings/Settings.h>
#include <UI/Button.h>
#include <Util/Util.h>

using namespace Platy::Game;
using namespace Graphics;

int main()
{
#if TEST_LOG || TEST_GAME_CORE
	Platy::Log::Init();
#endif

#if TEST_LOG
	Platy::Log::Debug("Test msg");
	Platy::Log::Warning("Test msg");
	Platy::Log::Critical("Test msg");
	Platy::Log::Critical(std::exception("Test exception"), "Test exception was thrown!");
	Platy::Log::Information("Test msg");
#endif


#if TEST_CORE

	TestObserver observer;

	TestObservable observable(&observer, 5);
	observable.UpdateData(10);

	auto* observerPtr = new TestObserver();

	TestObservable observable2(observerPtr, 5);
	observable2.UpdateData(10);

	delete observerPtr;
	observerPtr = nullptr;

#endif

#if TEST_GAME_CORE
	srand(static_cast<unsigned>(time(nullptr)));

	sf::RenderWindow window;
	window.create(sf::VideoMode(1280, 720), "Platy Testing Grounds");
	window.setVerticalSyncEnabled(false);
	window.setFramerateLimit(144);

	sf::Image icon;
	icon.loadFromFile("Assets/icon.png");
	window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());

	/// Init clock for delta time
	sf::Clock tempClock;
	float deltaTime;
	// Init singletons
	Platy::Log::Init();
	PostMaster::Init();
	AssetContainer::Init();
	StatusTextFactory::Init("firstorder");
	Settings::Init();

	ParticleTest tempParticleTest;
	StatusTextTest tempStatusTextTest{};

	Button testBtn(sf::Vector2f(500.f, 300.f), sf::Vector2f(150.f, 30.f), "Test button", "firstorder");

	auto tempAnimator = Animator(AssetContainer::GetSpriteSheet("tailstest"), 1);
	auto tempAnimPos = sf::Vector2f(300, 300);
	tempAnimator.Flip();

	while (window.isOpen())
	{
		EventHandler::HandleEvent(window);

		deltaTime = tempClock.restart().asSeconds();

		//////////////////////////////////////////////////////////////////
		// Update logic
		//////////////////////////////////////////////////////////////////

		StatusTextManager::Update(deltaTime);
		ParticleManager::Update(deltaTime);

		tempStatusTextTest.Update(deltaTime);
		tempParticleTest.Update(deltaTime);

		tempAnimPos = Util::Lerp(tempAnimPos, sf::Vector2f(800, 700), deltaTime / 2.f);

		tempAnimator.Update(deltaTime, tempAnimPos);
		//////////////////////////////////////////////////////////////////
		// Draw logic
		//////////////////////////////////////////////////////////////////
		window.clear();

		ParticleManager::EarlyDraw(window);

		window.draw(tempAnimator);

		//window.draw(testBtn);

		//StatusTextManager::Draw(window);

		ParticleManager::Draw(window);

		window.display();
		//////////////////////////////////////////////////////////////////
	}
#endif

	return 0;
}
