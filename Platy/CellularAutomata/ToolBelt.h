#ifndef TOOLBELT_H
#define TOOLBELT_H
#include <SFML/System/Vector2.hpp>

#include "Postmaster/Subscriber.h"

class Sandbox;

class ToolBelt : public Platy::Game::Subscriber
{
public:
	explicit ToolBelt(Sandbox* sim, int scaleFactor);
	~ToolBelt() override;

	void Update() const;
	
	void ReceiveMessage(const Platy::Game::EMessageType& aMessageType) override;
	void ReceiveMessage(const Platy::Game::Message& aMessage, const Platy::Game::EMessageType& aMessageType) override;
private:

	int myBrush;
	int myBrushSize;
	int myScaleFactor;

	bool myIsBrushing;
	sf::Vector2i myBrushPos;
	
	Sandbox* mySim;
};
#endif
