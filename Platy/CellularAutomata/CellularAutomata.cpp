// Sandbox.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <SFML/Graphics/RenderWindow.hpp>

#include "ParticleDefinitions.h"
#include "Events/EventHandler.h"
#include "Postmaster/PostMaster.h"

#include "Sandbox.h"
#include "ToolBelt.h"

using namespace Platy;

int main()
{
	srand(static_cast<unsigned>(time(nullptr)));

	sf::RenderWindow window;
	window.create(sf::VideoMode(1280, 720), "Sandbox");
	window.setVerticalSyncEnabled(false);
	window.setFramerateLimit(60);

	sf::Clock clock;
	float deltaTime;

	Game::PostMaster::Init();
	
	Sandbox sim(static_cast<int>(window.getSize().x) / SCALE_FACTOR, static_cast<int>(window.getSize().y) / SCALE_FACTOR, SCALE_FACTOR);
	ToolBelt tool(&sim, SCALE_FACTOR);
	
	while (window.isOpen())
	{
		deltaTime = clock.restart().asSeconds();
		
		Game::EventHandler::HandleEvent(window);

		sim.Update();
		tool.Update();
		
		window.clear();
		window.draw(sim);

		window.display();
	}

	return 0;
}
