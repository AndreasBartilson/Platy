#include "Sandbox.h"

#include <iostream>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

#include "Particle.h"
#include "ParticleDefinitions.h"
#include "Util/Util.h"

using namespace Platy::Core;

Sandbox::Sandbox(const int width, const int height, const int particleSize) :
	myParticleVertices(sf::PrimitiveType::Quads, static_cast<unsigned long long>(width) * height * 4L),
	myWidth(width),
	myHeight(height),
	myParticleSize(particleSize)
{	
	InitBuffer(width, height);
	InitDisplay(width, height);
}

Sandbox::~Sandbox()
{
	delete[] myBuffer;
	myBuffer = nullptr;
}

void Sandbox::Update()
{
	for (auto x = myWidth - 1; x >= 0; --x)
	{
		for (auto y = myHeight - 1; y >= 0; --y)
		{
			auto& p = myBuffer[x][y];
			if (p.id != AIR && !p.updated)
			{
				UpdateParticle(x, y);
			}
		}
	}

	for (auto i = 0; i < myWidth; ++i)
	{
		for (auto j = 0; j < myHeight; ++j)
		{
			myBuffer[i][j].updated = false;
		}
	}
}

void Sandbox::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.texture = nullptr;
	target.draw(myParticleVertices, states);
}

void Sandbox::SetParticle(const int x, const int y, const int value)
{
	if (x >= 0 && x < myWidth && y >= 0 && y < myHeight) {
		myBuffer[x][y] = Particle(value);

		switch(value)
		{
		case WALL:
		case STONE:
		case WOOD:
			myBuffer[x][y].solid = true;
			break;
		case LAVA:
			myBuffer[x][y].temperature = 100;
			break;
		case SMOKE:
			myBuffer[x][y].life = 500;
			break;
		default:
			break;
		}
		UpdateColor(x, y);
	}
}

int Sandbox::GetParticleId(const int x, const int y) const
{
	if (x < 0 || x >= myWidth || y < 0 || y >= myHeight)
	{
		return WALL;
	}
	return myBuffer[x][y].id;
}

void Sandbox::UpdateParticle(const int x, const int y)
{
	switch (myBuffer[x][y].id)
	{
	default:
		return;
	case SAND:
		UpdateSand(x, y);
		break;
	case WATER:
		UpdateWater(x, y);
		break;
	case LAVA:
		UpdateLava(x, y);
		break;
	case SMOKE:
		UpdateSmoke(x, y);
		break;
	case STONE:
		UpdateStone(x, y);
		break;
	case WOOD:
		UpdateWood(x, y);
		break;
	case ASH:
		UpdateAsh(x, y);
		break;
	}
}

void Sandbox::InitBuffer(const int width, const int height)
{
	myBuffer = new Particle* [static_cast<unsigned long long>(height) * width];
	for (auto x = 0; x < width; ++x)
	{
		myBuffer[x] = new Particle[height];
		for (auto y = 0; y < height; ++y)
		{
			myBuffer[x][y] = Particle(AIR);
		}
	}
}

void Sandbox::InitDisplay(const int width, const int height)
{
	for (auto x = 0; x < width; ++x)
	{
		for (auto y = 0; y < height; ++y)
		{	
			auto* quad = GetQuad(x, y);
			quad[0].position = sf::Vector2f(static_cast<float>(x) * static_cast<float>(myParticleSize),
			                                static_cast<float>(y) * static_cast<float>(myParticleSize));
			quad[1].position = sf::Vector2f(
				static_cast<float>(x) * static_cast<float>(myParticleSize) + static_cast<float>(myParticleSize),
				static_cast<float>(y) * static_cast<float>(myParticleSize));
			quad[2].position = sf::Vector2f(
				static_cast<float>(x) * static_cast<float>(myParticleSize) + static_cast<float>(myParticleSize),
				static_cast<float>(y) * static_cast<float>(myParticleSize) + static_cast<float>(myParticleSize));
			quad[3].position = sf::Vector2f(
				static_cast<float>(x) * static_cast<float>(myParticleSize), 
				static_cast<float>(y) * static_cast<float>(myParticleSize) + static_cast<float>(myParticleSize));
			UpdateColor(x, y);
		}
	}
}

void Sandbox::UpdateSand(const int x, const int y)
{
	// Down
	if (DenserThan(x, y, x, y + 1) && WithinBounds(x, y + 1))
	{
		Swap(x, y, x, y + 1);
	}
	else
	{
		// Attempt diagonal
		if(Util::RandBool())
		{
			for (auto i = 3; i >= 1; --i)
			{
				if (WithinBounds(x - i, y + i) && myBuffer[x - i][y + i].id == AIR)
				{
					Swap(x - i, y + i, x, y);
					return;
				}
			}
			
			// Down to the left
			if (DenserThan(x, y, x - 1, y + 1) && WithinBounds(x - 1, y + 1))
			{
				Swap(x, y, x - 1, y + 1);
			}
		}
		else
		{
			for (auto i = 3; i >= 1; --i)
			{
				if (WithinBounds(x + i, y + i) && myBuffer[x + i][y + i].id == AIR)
				{
					Swap(x + i, y + i, x, y);
					return;
				}
			}
			
			// Down to the right
			if (DenserThan(x, y, x + 1, y + 1) && WithinBounds(x + 1, y + 1))
			{
				Swap(x, y, x + 1, y + 1);
			}
		}
	}
}

void Sandbox::UpdateWater(const int x, const int y, const int invViscosity)
{
	// Down
	if (DenserThan(x, y, x, y + 1) && WithinBounds(x, y + 1))
	{
		Swap(x, y, x, y + 1);
	}
	else
	{
		// Attempt diagonal
		if (Util::RandBool())
		{
			for (auto i = invViscosity; i >= 1; --i)
			{
				if (WithinBounds(x - i,y + i) && myBuffer[x - i][y + i].id == AIR)
				{
					Swap(x - i, y + i, x, y);
					return;
				}
			}
			
			if (DenserThan(x, y, x - 1, y + 1) && WithinBounds(x - 1, y + 1))
			{
				Swap(x, y, x - 1, y + 1);
				return;
			}
		}
		else 
		{
			for (auto i = invViscosity; i >= 1; --i)
			{
				if (WithinBounds(x + i, y + i) && myBuffer[x + i][y + i].id == AIR)
				{
					Swap(x + i, y + i, x, y);
					return;
				}
			}
			
			if (DenserThan(x, y, x + 1, y + 1) && WithinBounds(x + 1, y + 1))
			{
				Swap(x, y, x + 1, y + 1);
				return;
			}
		}

		// Attempt sideways
		if (Util::RandBool())
		{
			for (auto i = invViscosity; i >= 1; --i)
			{
				if (WithinBounds(x - i, y) && myBuffer[x - i][y].id == AIR)
				{
					Swap(x - i, y, x, y);
					return;
				}
			}
			
			if (DenserThan(x, y, x - 1, y) && WithinBounds(x - 1, y))
			{
				Swap(x, y, x - 1, y);
			}
		}
		else
		{
			for (auto i = invViscosity; i >= 1; --i)
			{
				if (WithinBounds(x + i, y) && myBuffer[x + i][y].id == AIR)
				{
					Swap(x + i, y, x, y);
					return;
				}
			}
			
			if (DenserThan(x, y, x + 1, y) && WithinBounds(x + 1, y))
			{
				Swap(x, y, x + 1, y);
			}
		}
	}
}

void Sandbox::UpdateLava(const int x, const int y)
{
	// Randomly emit smoke
	if (GetParticleId(x, y - 1) == AIR && Util::RandInt(0, 100) > 98)
	{
		SetParticle(x, y - 1, SMOKE);
	}
	
	// Replace nearby water with smoke
	for (auto i = - 1; i < 2; ++i)
	{
		for (auto j = -1; j < 2; ++j)
		{
			if (GetParticleId(x + i, y + j) == WATER)
			{
				SetParticle(x + i, y + j, SMOKE);
				myBuffer[x + i][y + j].updated = true;
				myBuffer[x][y].temperature--;
				UpdateColor(x, y);
				if (myBuffer[x][y].temperature <= 0)
				{
					SetParticle(x, y, STONE);
				}
			}
			else if(GetParticleId(x + i, y + j) == WOOD)
			{
				myBuffer[x + i][y + j].isBurning = true;
			}
			else if (GetParticleId(x + i, y + j) == LAVA)
			{
				if (myBuffer[x + i][y + j].temperature > myBuffer[x][y].temperature && Util::RandInt(0, 100) > 99)
				{
					myBuffer[x + i][y + j].temperature--;
					//myBuffer[x][y].temperature++;
					UpdateColor(x, y);
					UpdateColor(x + i, y + j);
				}
			}
		}
	}

	UpdateWater(x, y, 1);
}

void Sandbox::UpdateSmoke(const int x, const int y)
{
	myBuffer[x][y].life--;
	if (myBuffer[x][y].life <= 0)
	{
		SetParticle(x, y, AIR);
		return;
	}
	
	// Up
	if (DenserThan(x, y - 1, x, y) && WithinBounds(x, y - 1) && !myBuffer[x][y - 1].solid)
	{
		Swap(x, y, x, y - 1);
	}
	else
	{
		// Attempt diagonal
		if (Util::RandBool())
		{
			for (auto i = 0; i < 3; ++i)
			{
				if (WithinBounds(x - i, y - i) && myBuffer[x - i][y - i].id == AIR)
				{
					Swap(x - i, y - i, x, y);
					return;
				}
			}

			if (DenserThan(x - 1, y - 1, x, y) && WithinBounds(x - 1, y - 1) && !myBuffer[x - 1][y - 1].solid)
			{
				Swap(x, y, x - 1, y - 1);
				return;
			}
		}
		else
		{
			for (auto i = 0; i < 3; ++i)
			{
				if (WithinBounds(x + i, y - i) && myBuffer[x + i][y - i].id == AIR)
				{
					Swap(x + i, y - i, x, y);
					return;
				}
			}
			
			if (DenserThan(x + 1, y - 1, x, y) && WithinBounds(x + 1, y - 1) && !myBuffer[x + 1][y - 1].solid)
			{
				Swap(x, y, x + 1, y - 1);
				return;
			}
		}

		// Attempt sideways
		if (Util::RandBool())
		{
			if (DenserThan(x - 1, y, x, y) && WithinBounds(x - 1, y) && !myBuffer[x - 1][y].solid)
			{
				Swap(x, y, x - 1, y);
			}
		}
		else
		{
			if (DenserThan(x + 1, y, x, y) && WithinBounds(x + 1, y) && !myBuffer[x + 1][y].solid)
			{
				Swap(x, y, x + 1, y);
			}
		}
	}
}

void Sandbox::UpdateStone(const int x, const int y)
{
	/*if (Util::RandInt(0, 100) < 97)
	{
		return;
	}
	
	const auto isDenser = DenserThan(x, y, x, y + 1);
	// Down
	if (isDenser && WithinBounds(x, y + 1))
	{
		Swap(x, y, x, y + 1);
	}
	else if (!isDenser)
	{
		// Attempt diagonal
		if (Util::RandBool())
		{
			// Down to the left
			if (DenserThan(x, y, x - 1, y + 1) && WithinBounds(x - 1, y + 1))
			{
				Swap(x, y, x - 1, y + 1);
			}
		}
		else 
		{
			// Down to the right
			if (DenserThan(x, y, x + 1, y + 1) && WithinBounds(x + 1, y + 1))
			{
				Swap(x, y, x + 1, y + 1);
			}
		}
	}*/
}

void Sandbox::UpdateWood(const int x, const int y)
{
	if (myBuffer[x][y].isBurning)
	{
		myBuffer[x][y].temperature++;
		UpdateColor(x, y);

		if (GetParticleId(x, y - 1) == AIR && Util::RandInt(0, 100) > 96)
		{
			SetParticle(x, y - 1, SMOKE);
		}
		
		if (myBuffer[x][y].temperature >= 250)
		{
			if (Util::RandInt(0, 100) > 85) {
				SetParticle(x, y, ASH);
			}
			else
			{
				SetParticle(x, y, AIR);
			}
		}
		else
		{
			for (auto i = -1; i < 2; ++i)
			{
				for (auto j = -1; j < 2; ++j)
				{
					if (WithinBounds(x + i, y + j) && GetParticleId(x + i, y + j) == WOOD)
					{
						if (Util::RandInt(0, 200) > 198)
						{
							myBuffer[x + i][y + j].isBurning = true;
						}
					}
				}
			}
		}
	}
}

void Sandbox::UpdateAsh(const int x, const int y)
{
	UpdateSand(x, y);
}

bool Sandbox::DenserThan(const int ax, const int ay, const int bx, const int by) const
{
	return DENSITY[GetParticleId(ax, ay)] > DENSITY[GetParticleId(bx, by)];
}

bool Sandbox::WithinBounds(const int x, const int y) const
{
	return x >= 0 && x < myWidth&& y >= 0 && y < myHeight;
}

void Sandbox::UpdateColor(const int x, const int y)
{
	sf::Color color;

	switch (myBuffer[x][y].id)
	{
	case AIR:
		color = sf::Color::Black;
		break;
	case WALL:
		color = COLOR_WALL;
		break;
	case SAND:
		color = COLOR_SAND;
		break;
	case WATER:
		color = COLOR_WATER;
		break;
	case LAVA:
		color = COLOR_LAVA;
		color.a = static_cast<sf::Uint8>(255 - 20 - myBuffer[x][y].temperature);
		break;
	case SMOKE:
		color = COLOR_WALL;
		color.a = 255 / 8;
		break;
	case STONE:
		color = COLOR_STONE;
		break;
	case WOOD:
		color = COLOR_WOOD;
		color.r = 40 + myBuffer[x][y].temperature / 2;
		break;
	case ASH:
		color = COLOR_ASH;
		break;
	default: 
		break;
	}
	
	auto* quad = GetQuad(x, y);
	quad[0].color = color;
	quad[1].color = color;
	quad[2].color = color;
	quad[3].color = color;
}

void Sandbox::Swap(const int ax, const int ay, const int bx, const int by)
{
	const auto temp = myBuffer[ax][ay];
	myBuffer[ax][ay] = myBuffer[bx][by];
	myBuffer[bx][by] = temp;

	myBuffer[ax][ay].updated = true;
	myBuffer[bx][by].updated = true;

	UpdateColor(ax, ay);
	UpdateColor(bx, by);
}

sf::Vertex* Sandbox::GetQuad(const int x, const int y)
{
	return &myParticleVertices[(static_cast<unsigned long long>(x) + static_cast<unsigned long long>(y) * myWidth) * 4];
}
