#include "ToolBelt.h"

#include "ParticleDefinitions.h"
#include "Sandbox.h"
#include "Postmaster/MessageType.h"
#include "Postmaster/Message.h"

using namespace Platy::Game;

ToolBelt::ToolBelt(Sandbox* sim, const int scaleFactor) : myBrush(AIR), myBrushSize(10), myScaleFactor(scaleFactor), mySim(sim), myIsBrushing(false)
{
	Subscribe(EMessageType::KEY_PRESSED);
	Subscribe(EMessageType::MOUSE_LEFT_PRESSED);
	Subscribe(EMessageType::MOUSE_LEFT_RELEASED);
	Subscribe(EMessageType::MOUSE_MOVED);
}

ToolBelt::~ToolBelt()
{
	RemoveAllSubscriptions();
	mySim = nullptr;
}

void ToolBelt::Update() const
{
	if (myIsBrushing)
	{
		for (auto i = 0; i < myBrushSize; ++i)
		{
			for (auto j = 0; j < myBrushSize; ++j)
			{
				mySim->SetParticle(myBrushPos.x / myScaleFactor- myBrushSize / 2 + i, myBrushPos.y / myScaleFactor - myBrushSize / 2 + j, myBrush);
			}
		}
	}
}

void ToolBelt::ReceiveMessage(const EMessageType& aMessageType)
{
}

void ToolBelt::ReceiveMessage(const Message& aMessage, const EMessageType& aMessageType)
{

	switch (aMessageType)
	{
	case EMessageType::KEY_PRESSED:
		switch (aMessage.GetKey())
		{
		case sf::Keyboard::Key::Num1:
			myBrush = AIR;
			break;
		case sf::Keyboard::Key::Num2:
			myBrush = WALL;
			break;
		case sf::Keyboard::Key::Num3:
			myBrush = SAND;
			break;
		case sf::Keyboard::Key::Num4:
			myBrush = WATER;
			break;
		case sf::Keyboard::Key::Num5:
			myBrush = LAVA;
			break;
		case sf::Keyboard::Key::Num6:
			myBrush = SMOKE;
			break;
		case sf::Keyboard::Key::Num7:
			myBrush = STONE;
			break;
		case sf::Keyboard::Key::Num8:
			myBrush = WOOD;
			break;
		case sf::Keyboard::Key::Num9:
			myBrush = ASH;
			break;
		default:
			break;
		}
		break;

	case EMessageType::MOUSE_LEFT_PRESSED:
		myIsBrushing = true;
		break;

	case EMessageType::MOUSE_LEFT_RELEASED:
		myIsBrushing = false;
		break;

	case EMessageType::MOUSE_MOVED:
		myBrushPos = aMessage.GetPosition();
		break;
	default:
		break;
	}
}
