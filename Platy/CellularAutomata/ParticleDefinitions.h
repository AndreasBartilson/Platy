#ifndef PARTICLEDEFS_H
#define PARTICLEDEFS_H

constexpr int SCALE_FACTOR = 8;

#define COLOR_SAND sf::Color(194, 178, 128)
#define COLOR_WATER sf::Color::Blue //sf::Color(212, 241, 249)
#define COLOR_WALL sf::Color(169, 169, 169)
#define COLOR_LAVA sf::Color(128, 0, 0)
#define COLOR_STONE sf::Color(90, 90, 90)
#define COLOR_WOOD sf::Color(40, 26, 13)
#define COLOR_ASH sf::Color(211, 211, 211)

constexpr int AIR = 0;
constexpr int WALL = 1;
constexpr int SAND = 2;
constexpr int WATER = 3;
constexpr int SMOKE = 4;
constexpr int LAVA = 5;
constexpr int STONE = 6;
constexpr int WOOD = 7;
constexpr int ASH = 8;

constexpr int DENSITY[]
{
	10,		 // Air
	1000000, // Wall
	1600,	 // Sand	
	997,	 // Water
	9,		 // Smoke
	3100,	 // Lava
	3400,	 // Stone
	1000000, // Wood
	1000	 // Ash
};

#endif
