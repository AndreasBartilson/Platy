﻿#ifndef PARTICLE_H
#define PARTICLE_H

#include <SFML/Graphics/Color.hpp>

class Particle
{
public:
	Particle();
	Particle(int id);
	//Particle(const Particle& other) = delete;
	//Particle(Particle&& other) noexcept = delete;
	~Particle() = default;

	//Particle& operator=(const Particle& other) = delete;
	//Particle& operator=(Particle&& other) noexcept;

	int life;
	int id{};
	int temperature{};
	float vx{}, vy{};
	bool updated{};
	bool solid{};
	bool isBurning{};
	sf::Color color;
};
#endif
