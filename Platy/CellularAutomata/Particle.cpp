﻿#include "Particle.h"

Particle::Particle() = default;

Particle::Particle(const int id) : life(0), id(id), temperature(0)
{
	isBurning = false;
	solid = false;
	color = sf::Color::Black;
}
