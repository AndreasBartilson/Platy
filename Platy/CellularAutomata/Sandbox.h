#ifndef SANDSIMULATOR_H
#define SANDSIMULATOR_H

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/VertexArray.hpp>

class Particle;

namespace sf
{
	class RectangleShape;
}

class Sandbox final : public sf::Drawable
{
public:
	Sandbox(int width, int height, int particleSize);
	Sandbox(const Sandbox& other) = delete;
	Sandbox(Sandbox&& other) = delete;
	~Sandbox() override;

	void Update();

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	void SetParticle(int x, int y, int value);

	Sandbox& operator=(const Sandbox& other) = delete;
	Sandbox& operator=(Sandbox&& other) = delete;

private:

	void InitBuffer(int width, int height);
	void InitDisplay(int width, int height);

	int GetParticleId(int x, int y) const;

	void UpdateParticle(int x, int y);
	void UpdateSand(int x, int y);
	void UpdateWater(int x, int y, int invViscosity = 5);
	void UpdateLava(int x, int y);
	void UpdateSmoke(int x, int y);
	void UpdateStone(int x, int y);
	void UpdateWood(int x, int y);
	void UpdateAsh(int x, int y);

	/// <summary>
	/// Compare density of two materials with respect to sandbox dimensions
	/// </summary>
	/// <returns>True if a is denser than b</returns>
	bool DenserThan(int ax, int ay, int bx, int by) const;

	bool WithinBounds(int x, int y) const;
	
	void UpdateColor(int x, int y);
	void Swap(int ax, int ay, int bx, int by);
	
	Particle** myBuffer{};
	sf::VertexArray myParticleVertices;
	
	int myWidth;
	int myHeight;
	int myParticleSize;

	sf::Vertex* GetQuad(int x, int y);
};
#endif
